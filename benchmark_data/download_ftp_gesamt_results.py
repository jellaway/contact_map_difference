# from importlib.resources import path
# from ftplib import ftpcp
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import requests
import pathlib





def download_file(url, save_dir):

    r = requests.get(url, allow_redirects=True)

    if r.status_code != 200:
        print("Request for", url, "failed with status:")
        print(r.status_code)
        return None
    else:
        open(save_dir, "wb").write(r.content)
    # save_to = save_dir.joinpath(mmcif_file_name)
        





if __name__ == "__main__":

    path_home = pathlib.Path.home()

    path_bm_df = path_home.joinpath(
        "EMBL-EBI", 
        "funclan_work",
        "static-conformer-dataset",
        "open_closed_mancur_data",
        "benchmark_monomeric_open_closed_conformers.csv"
    )

    path_save = path_home.joinpath(
        "EMBL-EBI", 
        "funclan_work",
        "static-conformer-dataset",
        "gesamt_outputs"
    )

    bm_df = pd.read_csv(path_bm_df)

    count = 0

    df_size = len(bm_df["UNP_ACC"].unique())

    ftp_url = "http://ftp.ebi.ac.uk/pub/databases/pdbe-kb/superposition"
    
    for unp in bm_df["UNP_ACC"].unique():

        # print(bm_df["UNP_ACC"].iloc[count], 
        #             bm_df["CONFORMER_ID"].iloc[count], 
        #             bm_df["PDBe_ID"].iloc[count]
        #             )

        for i in range(0, 6):
            save_dir = path_save.joinpath(f"{unp}_segment{i}_gesamt_output.txt")
            tmp_ftp_url = f"{ftp_url}/{unp[0]}/{unp}/segment{i}/Gesamt_Output.out"

            download_file(tmp_ftp_url, save_dir)

        count += 1
