

import pickle
import pandas as pd
import os 
import pathlib
import sys
import numpy as np
from matplotlib import pyplot as plt

sys.path[0] = str(                      # Override default Python import part
    pathlib.Path(
       os.path.realpath(__file__)       # Path to this file
        ).parent.parent                 # Path to folder this file is in
    )

from utils import io_utils, logging_utils
"""
Processes to perform analyses on the clustering results across the whole archive

Things to discover:
 - Distribution of cluster number per UniProt segment, before and after 
 - Contribution of AlphaFold structures -- more, few, different clusters?
 - Do number of unique clusters change -- how many are new and how many are unchanged?

 - Number of chains per cluster
 - Number of chains per UniProt segment

 - Text mining of entry headers for information on conformational types
 - Determine how many are borderline -- within x% of cutoff
    - How different the clusters are 
    - Examples of small and large changes
 - With and without literature support
 - Type of experimental technique used for each conformer
 - Model certainty/confidence in clusters
    - Ramachandran angles
    - clashscore
    - Sidechain outliers
 - Ligand binding
 - Complex/free subunits between cluster
 - Assign Sterling numbers of the second kind in order to find proberbility of 
    clustering correctly

Job submitted to queue: 9557252
"""


def init_empty_csv(csv_path :pathlib.Path):

    # Add parent file if not already present
    parent_path = csv_path.parent
    parent_path.mkdir(parents=True, exist_ok=True)

    command = f"touch { str(csv_path) }"
    os.system(command)



def collect_clustering_csvs(path_data :pathlib.Path, path_aggregate_csv :pathlib.Path):
    """Works

    :param path_data: _description_
    :type path_data: pathlib.Path
    :param path_aggregate_csv: _description_
    :type path_aggregate_csv: pathlib.Path
    """


    path_aggregate_csv = str(path_aggregate_csv)
    # A, B, C, ...
    unp_headers = io_utils.get_fnames(path=path_data)
    for unp_header in unp_headers:

        path_unp_header = path_data.joinpath(unp_header)

        # A12345, A67890, ...
        unps = io_utils.get_fnames( path=path_unp_header )

        for unp in unps:
            # A12345_clusters.csv
            path_csv = path_unp_header.joinpath(
                unp, 
                f"{unp}_clusters.csv"
            )

            if path_csv.is_file():

                os.system(
                    f"cat {str(path_csv)} | grep {unp} >> {path_aggregate_csv}"
                )
                print(path_csv)



def num_chains_per_cluster(df):
    """Calculates the number of chains per cluster for a given dataframe of clustering
    results. 

    :param df: _description_
    :type df: _type_
    """

    # over_10_counts = []
    # between_5_10_counts = []
    counts = []

    progress = logging_utils.ProgressBar(len(df["uniprot_accession"].unique()))

    for unp in df["uniprot_accession"].unique():

        df_unp = df[df["uniprot_accession"] == unp]

        for cluster_id_segment in df_unp["structural_cluster_id"].unique():

            df_unp_segm = df_unp[df_unp["structural_cluster_id"] == cluster_id_segment]

            num_chains = df_unp_segm.shape[0]

            # if num_conformers >= 10:
            #     over_10_counts.append(unp)
            # elif num_conformers >= 5:
            #     between_5_10_counts.append(unp)

            counts.append(num_chains)
        
        progress.update()
        
    pickle.dump(counts, open("num_chains_per_cluster_qs.pickle", "wb"))

    # pickle.dump(over_10_counts, open("qs_over_10_counts.pickle", "wb"))
    # pickle.dump(between_5_10_counts, open("qs_between_5_10_counts.pickle", "wb"))



def cluster_frequency_distribution(df):

    over_10_counts = []
    between_5_10_counts = []
    counts = []

    progress = logging_utils.ProgressBar(len(df["uniprot_accession"].unique()))

    for unp in df["uniprot_accession"].unique():

        df_unp = df[df["uniprot_accession"] == unp]

        for segment in df_unp["segment_sequence_ranges"].unique():

            df_unp_segm = df_unp[df_unp["segment_sequence_ranges"] == segment]

            num_conformers = len(df_unp_segm["structural_cluster_id"].unique())

            if num_conformers >= 10:
                over_10_counts.append(unp)
            elif num_conformers >= 5:
                between_5_10_counts.append(unp)

            counts.append(num_conformers)
        
        progress.update()
        
    pickle.dump(counts, open("qs_all_counts.pickle", "wb"))
    pickle.dump(over_10_counts, open("qs_over_10_counts.pickle", "wb"))
    pickle.dump(between_5_10_counts, open("qs_between_5_10_counts.pickle", "wb"))

# ['P04585', 'P08581', 'P08581-3', 'P0DP23', 'P11362', 'P11362-8', 'P30613', 'P30613-2', 'Q99ZW2', 'Q9Y230']


# def cluster_frequency_distribution(df):

#     value_counts = df.value_counts(
#         subset=[
#             "uniprot_accession", 
#             "structural_cluster_id"
#         ]
#     )

#     pickle.dump(value_counts, open("value_counts.pickle", "wb"))



def compare_cluster_num(df_a, df_b):

    pc_changes = []

    progress = logging_utils.ProgressBar(len(df_a["uniprot_accession"].unique()))

    for unp in df_b["uniprot_accession"].unique():

        df_unp_a = df_a[df_a["uniprot_accession"] == unp]
        df_unp_b = df_b[df_b["uniprot_accession"] == unp]

        for segment in df_unp_b["segment_sequence_ranges"].unique():

            df_unp_segm_a = df_unp_a[df_unp_a["segment_sequence_ranges"] == segment]
            num_conformers_a = len(df_unp_segm_a["structural_cluster_id"].unique())

            df_unp_segm_b = df_unp_b[df_unp_b["segment_sequence_ranges"] == segment]
            num_conformers_b = len(df_unp_segm_b["structural_cluster_id"].unique())

            pc_change = (num_conformers_a - num_conformers_b) / num_conformers_b

            pc_changes.append(pc_change)
        
        progress.update()
    
    pickle.dump(pc_changes, open("num_confs_pc_change.pickle", "wb") )






if __name__ == "__main__":

    # path_base = pathlib.Path(
    #     "/hps",
    #     "nobackup",
    #     "pdbe",
    #     "users",
    #     "jellaway"
    # )
    # path_results = path_base.joinpath(
    #     "superpose_220909",
    #     "gesamt"
    # )
    # path_output_csv = path_base.joinpath("aggregate_clustering_results_220909.csv")
    # init_empty_csv(path_output_csv)
    # collect_clustering_csvs(path_results, path_output_csv)

    # # test
    # path_results = pathlib.Path(
    #     "/Users/jellaway/EMBL-EBI/funclan_work/January39/gesamt/"
    # )

    # path_output_csv = pathlib.Path(
    #     "/Users/jellaway/EMBL-EBI/funclan_work/contact_map_difference/aggr_csv.csv"
    # )



    path_df_new = pathlib.Path(
        "/Users/jellaway/EMBL-EBI/funclan_work/aggregate_clustering_results_220909.csv"
    )

    path_df_old = pathlib.Path(
        "/Users/jellaway/EMBL-EBI/funclan_work/aggregate_clustering_results_qscore_220909.csv"
    )

    df_new = pd.read_csv(
        path_df_new, 
        names=[
            "pdb_id",
            "auth_asym_id",
            "struct_asym_id",
            "entity_id",
            "structural_cluster_id",
            "sequence_segment_id",
            "segment_begin",
            "segment_end",
            "segment_sequence_ranges",
            "uniprot_accession"
        ],
        header=None,
        index_col=False
    )

    df_old = pd.read_csv(
        path_df_old, 
        names=[
            "pdb_id",
            "auth_asym_id",
            "struct_asym_id",
            "entity_id",
            "structural_cluster_id",
            "sequence_segment_id",
            "segment_begin",
            "segment_end",
            "segment_sequence_ranges",
            "uniprot_accession"
        ],
        header=None,
        index_col=False
    )

    # # cluster_frequency_distribution(df)
    # num_chains_per_cluster(df)

    # compare_cluster_num(df_new, df_old)


    # # list_version_qs = pickle.load(open("qs_all_counts.pickle", "rb"))
    # list_version_dist = pickle.load(open("num_chains_per_cluster_qs.pickle", "rb"))
    list_version_pc_change = pickle.load(open("num_confs_pc_change.pickle", "rb"))

    # # over_10 = pickle.load(open("over_10_counts.pickle", "rb"))
    # # over_5 = pickle.load(open("between_5_10_counts.pickle", "rb"))

    # # print(over_10)
    # # print()
    # # print(over_5)


    plt.style.use("seaborn-colorblind")
    fig, ax = plt.subplots(1, 1)

    # ax.hist(list_version_qs, label="Reverse Q-score")
    ax.hist(list_version_pc_change, label="Reverse Q-score", bins=12)
    # ax1.vlines(
    #     x=[np.mean(list_version_qs), np.mean(list_version_qs)+np.std(list_version_qs)], 
    #     ymin=1, 
    #     ymax=100000,
    #     colors="black",
    #     linestyles="--"
    # )
    # ax2.hist(list_version_dist, label="Distance-based score", color="C1")
    # ax2.vlines(
    #     x=[np.mean(list_version_dist), np.mean(list_version_dist)+np.std(list_version_dist)], 
    #     ymin=1, 
    #     ymax=100000,
    #     colors="black",
    #     linestyles="--"
    # )

    # print(
    #     [np.median(list_version_qs), np.median(list_version_qs)+np.std(list_version_qs)],
    #     '\n',
    #     [np.median(list_version_dist), np.median(list_version_dist)+np.std(list_version_dist)])
    # ax.set_xlim(0, 16)
    # ax.set_ylim(0, 1e+5)

    # for ax in (ax1, ax2):
    ax.set_yscale("log")
    ax.set_xlabel("# Clusters per UniProt segment")
    ax.set_ylabel("# Frequency")
    ax.grid(zorder=0, alpha=0.2)
    ax.set_title("Distribution of cluster number percent. change", fontweight="bold")
    ax.legend()
    
    plt.savefig("conf_num_pc_change.png")
    plt.show()
    


    """
    Q951A5:
        6ynz
        6ynw
        6yny
    """







    # plt.style.use("seaborn-colorblind")
    # fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(15,5))

    # # ax.hist(list_version_qs, label="Reverse Q-score")
    # ax1.hist(list_version_qs, label="Reverse Q-score")
    # ax1.vlines(
    #     x=[np.mean(list_version_qs), np.mean(list_version_qs)+np.std(list_version_qs)], 
    #     ymin=1, 
    #     ymax=100000,
    #     colors="black",
    #     linestyles="--"
    # )
    # ax2.hist(list_version_dist, label="Distance-based score", color="C1")
    # ax2.vlines(
    #     x=[np.mean(list_version_dist), np.mean(list_version_dist)+np.std(list_version_dist)], 
    #     ymin=1, 
    #     ymax=100000,
    #     colors="black",
    #     linestyles="--"
    # )

    # print(
    #     [np.median(list_version_qs), np.median(list_version_qs)+np.std(list_version_qs)],
    #     '\n',
    #     [np.median(list_version_dist), np.median(list_version_dist)+np.std(list_version_dist)])
    # # ax.set_xlim(0, 16)
    # # ax.set_ylim(0, 1e+5)

    # for ax in (ax1, ax2):
    #     ax.set_yscale("log")
    #     ax.set_xlabel("# Clusters per UniProt segment")
    #     ax.set_ylabel("# Frequency")
    #     ax.grid(zorder=0, alpha=0.2)
    #     ax.set_title("Distribution of #cluster per UniProt segment", fontweight="bold")
    #     ax.legend()
    
    # plt.savefig("both_archive_distribution.png")
    # plt.show()
    


    # """
    # Q951A5:
    #     6ynz
    #     6ynw
    #     6yny
    # """


    

    








