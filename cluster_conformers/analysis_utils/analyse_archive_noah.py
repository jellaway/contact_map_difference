

import pickle
# import pandas as pd
import os 
import pathlib
import sys
import subprocess
# from matplotlib import pyplot as plt

sys.path[0] = str(                      # Override default Python import part
    pathlib.Path(
       os.path.realpath(__file__)       # Path to this file
        ).parent.parent                 # Path to folder this file is in
    )

"""
Processes to perform analyses on the clustering results across the whole archive

Things to discover:
 - Distribution of cluster number per UniProt segment, before and after 
 - Contribution of AlphaFold structures -- more, few, different clusters?
 - Do number of unique clusters change -- how many are new and how many are unchanged?
 - Number of chains per cluster
 - Number of chains per UniProt segment
 - Text mining of entry headers for information on conformational types
 - Determine how many are borderline -- within x% of cutoff
    - How different the clusters are 
    - Examples of small and large changes
 - With and without literature support
 - Type of experimental technique used for each conformer
 - Model certainty/confidence in clusters
    - Ramachandran angles
    - clashscore
    - Sidechain outliers
 - Ligand binding
 - Complex/free subunits between clusters
 - Assign Sterling numbers of the second kind in order to find proberbility of 
    clustering correctly

Job submitted to queue: 9557252
"""


def get_fnames(path):
    """
    Given a path to a dir containing files, returns a list of file names in the dir
    (including the path to their location prefixed) as strings.
    """

    ls = f"ls {path}"

    # Args to ensure correct formatnig of string elements
    args = {"shell" : True, 
            "encoding" : "utf-8"}

    return subprocess.check_output([ls], **args).splitlines()



def init_empty_csv(csv_path :pathlib.Path):

    # Add parent file if not already present
    parent_path = csv_path.parent
    parent_path.mkdir(parents=True, exist_ok=True)

    command = f"touch { str(csv_path) }"
    os.system(command)



def collect_clustering_csvs(path_data :pathlib.Path, path_aggregate_csv :pathlib.Path):
    """Works

    :param path_data: _description_
    :type path_data: pathlib.Path
    :param path_aggregate_csv: _description_
    :type path_aggregate_csv: pathlib.Path
    """


    path_aggregate_csv = str(path_aggregate_csv)
    # A, B, C, ...
    unp_headers = get_fnames(path=path_data)
    for unp_header in unp_headers:

        path_unp_header = path_data.joinpath(unp_header)

        # A12345, A67890, ...
        unps = get_fnames( path=path_unp_header )

        for unp in unps:
            # A12345_clusters.csv
            path_csv = path_unp_header.joinpath(
                unp, 
                f"{unp}_clusters.csv"
            )

            if path_csv.is_file():

                os.system(
                    f"cat {str(path_csv)} | grep {unp} >> {path_aggregate_csv}"
                )
                print(path_csv)






if __name__ == "__main__":

    path_base = pathlib.Path(
        "/homes",
        "msdsd"
    )

    path_results = path_base.joinpath(
        "/nfs",
        "ftp",
        "pub",
        "databases",
        "pdbe-kb",
        "superposition"
    )
    path_output_csv = path_base.joinpath("aggregate_clustering_results_qscore_220909.csv")
    init_empty_csv(path_output_csv)
    collect_clustering_csvs(path_results, path_output_csv)




