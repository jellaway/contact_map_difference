

import pickle
import pandas as pd
import os 
import pathlib
import sys
import numpy as np
from matplotlib import pyplot as plt

sys.path[0] = str(                      # Override default Python import part
    pathlib.Path(
       os.path.realpath(__file__)       # Path to this file
        ).parent.parent                 # Path to folder this file is in
    )

from utils import io_utils, logging_utils
"""
Processes to perform analyses on the clustering results across the whole archive

Things to discover:
 - Distribution of cluster number per UniProt segment, before and after 
 - Contribution of AlphaFold structures -- more, few, different clusters?
 - Do number of unique clusters change -- how many are new and how many are unchanged?

 - Number of chains per cluster
 - Number of chains per UniProt segment

 - Text mining of entry headers for information on conformational types
 - Determine how many are borderline -- within x% of cutoff
    - How different the clusters are 
    - Examples of small and large changes
 - With and without literature support
 - Type of experimental technique used for each conformer
 - Model certainty/confidence in clusters
    - Ramachandran angles
    - clashscore
    - Sidechain outliers
 - Ligand binding
 - Complex/free subunits between cluster
 - Assign Sterling numbers of the second kind in order to find proberbility of 
    clustering correctly

Job submitted to queue: 9557252
"""


if __name__ == "__main__":



    # # cluster_frequency_distribution(df)
    # num_chains_per_cluster(df)

    # compare_cluster_num(df_new, df_old)


    # # list_version_qs = pickle.load(open("qs_all_counts.pickle", "rb"))
    # list_version_dist = pickle.load(open("num_chains_per_cluster_qs.pickle", "rb"))

    num_clusters_per_segment_new = pickle.load(open("all_counts.pickle", "rb"))
    num_clusters_per_segment_old = pickle.load(open("qs_all_counts.pickle", "rb"))

    num_chains_per_cluster_new = pickle.load(open("num_chains_per_cluster_dist.pickle", "rb"))
    num_chains_per_cluster_old = pickle.load(open("num_chains_per_cluster_qs.pickle", "rb"))

    list_version_pc_change = pickle.load(open("num_confs_pc_change.pickle", "rb"))



    plt.style.use("seaborn-colorblind")
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(10, 7), tight_layout=True, sharey=True)

    # ax.hist(list_version_qs, label="Reverse Q-score")

    ax1.hist(num_clusters_per_segment_new, label="Distance-based score")
    ax1.vlines(
        x=[np.mean(num_clusters_per_segment_new), np.mean(num_clusters_per_segment_new)+np.std(num_clusters_per_segment_new)], 
        ymin=1, 
        ymax=100000,
        colors="black",
        linestyles="--"
    )
    ax1.set_xlabel("# Clusters per UniProt segment")
    ax1.set_title("Distribution of clusters per segment", fontweight="bold")


    ax2.hist(num_clusters_per_segment_old, label="Reverse Q-score", color="C1")
    ax2.vlines(
        x=[np.mean(num_clusters_per_segment_old), np.mean(num_clusters_per_segment_old)+np.std(num_clusters_per_segment_old)], 
        ymin=1, 
        ymax=100000,
        colors="black",
        linestyles="--"
    )
    ax2.set_xlabel("# Clusters per UniProt segment")
    ax2.set_title("Distribution of clusters per segment", fontweight="bold")

    ax3.set_title("")
    ax3.hist(num_chains_per_cluster_old, label="Reverse Q-score", alpha=0.7)
    ax3.hist(num_chains_per_cluster_new, label="Distance-based score", alpha=0.7)
    ax3.set_xlabel("# Chains per cluster")
    ax3.set_title("Distribution of chains per cluster", fontweight="bold")

    ax4.hist(list_version_pc_change)
    ax4.set_title("Distribution of cluster number % change per segment", fontweight="bold")
    ax4.set_xlabel("% change (/100)")



    # print(
    #     [np.median(list_version_qs), np.median(list_version_qs)+np.std(list_version_qs)],
    #     '\n',
    #     [np.median(list_version_dist), np.median(list_version_dist)+np.std(list_version_dist)])
    # ax.set_xlim(0, 16)
    # ax.set_ylim(0, 1e+5)

    for ax in (ax1, ax2, ax3, ax4):
        ax.set_yscale("log")
        ax.grid(zorder=0, alpha=0.2)
        ax.legend()

    for ax in (ax1, ax3):
        ax.set_ylabel("# Frequency")
    
    plt.savefig("all_comparison_figure.png")
    plt.show()
    






