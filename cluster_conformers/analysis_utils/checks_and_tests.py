
import pandas as pd
import pathlib
import os



if __name__ == "__main__":

    path_df = pathlib.Path(
        "/Users/jellaway/EMBL-EBI/funclan_work/distance_based_clustering_archive_results_short.csv"
    )

    df = pd.read_csv(path_df)

    df_P00519 = df[df["uniprot_accession"] == "P00519"]

    print(df_P00519[["segment_begin", "segment_end"]].drop_duplicates().shape)

    df_segm_1_3 = df_P00519[(df_P00519["segment_begin"] < 1007) & (df_P00519["segment_end"] <= 515) ]

    path_to_mmcifs = "/nfs/production/gerard/pdbe/release/data/entry/"

    row_index = 0

    while row_index < df_segm_1_3.shape[0]:

        # print(df_segm_1_3.iloc[row_index])

        # download_string = "/Users/jellaway/EMBL-EBI/funclan_work/"

        pdb = df_segm_1_3.iloc[row_index]["pdb_id"]
        chain = df_segm_1_3.iloc[row_index]["struct_asym_id"]

        print(f"-m ../P00519_updated_mmcifs/{pdb}_updated.cif.gz {chain} \\")

        # download_command = f"rsync -v jellaway@codon-login:{path_to_mmcifs}{pdb[1:3]}/{pdb}/sifts/{pdb}_updated.cif.gz ../P00519_updated_mmcifs"
        # os.system(download_command)

        row_index += 1