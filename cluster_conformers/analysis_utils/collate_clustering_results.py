
import pandas as pd
import pathlib


if __name__ == "__main__":

    path_df_new = pathlib.Path(
        "/Users/jellaway/EMBL-EBI/funclan_work/aggregate_clustering_results_220909.csv"
    )

    df = pd.read_csv(
        path_df_new, 
        names=[
            "pdb_id",
            "auth_asym_id",
            "struct_asym_id",
            "entity_id",
            "structural_cluster_id",
            "sequence_segment_id",
            "segment_begin",
            "segment_end",
            "segment_sequence_ranges",
            "uniprot_accession"
        ],
        header=None,
        index_col=False
    )

    df["cluster_id"] = df["structural_cluster_id"].str.slice(start=-1)

    df.drop(
        columns=[
            "structural_cluster_id", 
            "sequence_segment_id", 
            "segment_sequence_ranges"], 
        inplace=True
    )

    print(df.head().to_string())

    df.to_csv("../distance_based_clustering_archive_results_short.csv")