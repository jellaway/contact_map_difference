
import numpy as np
import pathlib
from matplotlib import pyplot as plt



def load_master_array(path, remove_zeros=True):

    array = np.load(path)
    # Remove NaNs
    array = array[np.logical_not(np.isnan(array))]

    if remove_zeros:
        array = array[array != 0]

    return array


def summary_stats(array, display=True):

    n =  len(array)
    mean = np.mean(array)
    median = np.median(array)
    stdev = np.std(array)

    if display:
        print("Summary statistics :")
        print("n = ", n)
        print("Mean = ", mean)
        print("Median = ", median)
        print("Stdev = ", stdev, '\n')

    return n, mean, median, stdev


if __name__ == "__main__":

    path_home = pathlib.Path.home()
    path_work = path_home.joinpath("EMBL-EBI", 
                                    "funclan-work", 
                                    "static-conformer-dataset"
                                    )

    path_intraconf_matxs = path_work.joinpath("intra_conformation_distance_difference")
    path_interconf_matxs = path_work.joinpath("inter_conformation_distance_difference")

    # fig_save_dir = root_dir + "/contact_map_difference/analysis"
    path_master_vectors = path_home.joinpath("EMBL-EBI", 
                                            "funclan-work", 
                                            "static-conformer-dataset", 
                                            "analysis"
                                            )



    path_intra_master_array = path_master_vectors.joinpath("benchmarking_all_intraconf_dist_diff_ignore_neighbours_4res.npy")
    intra_master_array = load_master_array(path_intra_master_array)
    
    path_inter_master_array = path_master_vectors.joinpath("benchmarking_all_interconf_dist_diff_ignore_neighbours_4res.npy")
    inter_master_array = load_master_array(path_inter_master_array)
    

    # # Printing summary stats to terminal
    # for i in (inter_master_array, intra_master_array):
    #     summary_stats_print(i)

    # Analytics:
    n_intra, mean_intra, median_intra, stdev_intra = summary_stats(intra_master_array)
    n_inter, mean_inter, median_inter, stdev_inter = summary_stats(inter_master_array)

    plt.style.use("seaborn-colorblind")
    # plt.ticklabel_format(style='plain')    # to prevent scientific notation.

    fig, ax = plt.subplots(1, 1)

    bin_num = 100
    max_height = 6e+7
    dashed_solid = ["--", "-"]

    ax.grid(alpha=0.4, linestyle="--")
    ax.hist(inter_master_array, bins=bin_num, label="Inter-conformation")
    ax.hist(intra_master_array, bins=bin_num, label="Intra-conformation")


    ax.vlines(mean_intra, 0, max_height, 
                                        colors="C2", 
                                        linestyles="--",
                                        label=u"Intra-conf. : \u03BC"
                                        )
    stats_label = u"\u03BC =" + f"{round(mean_intra, 3)}" + u" \u212B"
    ax.annotate(stats_label, (mean_intra-2, 4e+7), rotation="vertical")

    ax.vlines(mean_intra+stdev_intra, 0, max_height, 
                                        colors="C2", 
                                        linestyles="-",
                                        label=u"Intra-conf. : \u03BC+\u03C3",
                                        )

    stats_label = u"\u03BC+\u03C3 =" + f"{round(mean_intra+stdev_intra, 3)}" + u" \u212B"
    ax.annotate(stats_label, (mean_intra+stdev_intra-2, 4e+7), rotation="vertical")

    ax.vlines(mean_inter, 0, max_height, 
                                        colors="C3", 
                                        linestyles="--",
                                        label=u"Inter-conf. : \u03BC"
                                        )

    stats_label = u"\u03BC =" + f"{round(mean_inter, 3)}" + u" \u212B"
    ax.annotate(stats_label, (mean_inter+1, 4e+7), rotation="vertical")

    ax.vlines(mean_inter+stdev_inter, 0, max_height, 
                                        colors="C3", 
                                        linestyles="-",
                                        label=u"Inter-conf. : \u03BC+\u03C3",
                                        )
    stats_label = u"\u03BC+\u03C3 =" + f"{round(mean_inter+stdev_inter, 3)}" + u" \u212B"
    ax.annotate(stats_label, (mean_inter+stdev_inter+1, 4e+7), rotation="vertical")

    ax.ticklabel_format(style='plain')

    ax.set_xlabel(u"\u0394 Distance (\u212B)")
    ax.set_ylabel(u"Frequency (\u00d7 10\u2077)")
    ax.set_yticks([0, 1e+7, 2e+7, 3e+7, 4e+7, 5e+7, 6e+7])
    ax.set_ylim(0, 6e+7)
    ax.set_xlim(0, 60)
    ax.set_xticks([0, 10, 20, 30, 40, 50, 60])
    ax.set_yticklabels(["0", "1", "2", "3", "4", "5", "6"])

    # Annotations:

    ax.legend()

    ax.set_title("Intra- and inter-conformation CA distance difference", fontweight="bold")

    # Displaying figure
    # plt.show()





    # Distribution on all distance frequencies
    # plt.hist(intra_master_array, bins=100)
    # plt.xlabel("Distance (A)")
    # plt.ylabel("Frequency")
    # plt.show()
    plt.savefig("distance_difference_distribution_ignore_neighbours_4res.svg", format="svg")













    # print(len(as_vector))


