
import pandas as pd
from matplotlib import pyplot as plt
import pathlib
import numpy as np


if __name__ == "__main__":

    path_home = pathlib.Path.home()

    path_home = path_home.joinpath("EMBL-EBI", 
                                    "funclan_work", 
                                    "static-conformer-dataset", 
                                    "testing_dump", 
                                    "scores")

    ### MacBook paths ###
    # Path to updated mmcif files
    # path_updated_mmcifs = path_home.joinpath("updated_mmcif")

    intra_means = []
    intra_stdevs = []

    inter_means = []
    inter_stdevs = []

    for i in range(0, 26):
        path_intra = path_home.joinpath(f"intRA_conformation_score_info_mask_{i}.csv")
        path_inter = path_home.joinpath(f"intER_conformation_score_info_mask_{i}.csv")

        df_intra = pd.read_csv(path_intra)
        df_inter = pd.read_csv(path_inter)

        # Means
        intra_means.append( np.mean(df_intra["SCORE"]) )
        inter_means.append( np.mean(df_inter["SCORE"]) )

        # Standard deviations
        intra_stdevs.append( np.std(df_intra["SCORE"]) )
        inter_stdevs.append( np.std(df_inter["SCORE"]) )


    intra_means = np.asarray(intra_means)
    intra_stdevs = np.asarray(intra_stdevs)

    inter_means = np.asarray(inter_means)
    inter_stdevs = np.asarray(inter_stdevs)
        

    # PLotting
    plt.style.use("seaborn-colorblind")
    fig, ax = plt.subplots(1,1, tight_layout=True)

    ax.grid(alpha=0.3, linestyle="--")

    ax.plot(intra_means, label="Intra-conformation mean", color="C0")
    ax.plot(intra_means+intra_stdevs, color="C0", linestyle="--")
    ax.plot(intra_means-intra_stdevs, color="C0", linestyle="--")

    ax.fill_between(range(0, len(intra_means)), 
                        intra_means+intra_stdevs, 
                        intra_means-intra_stdevs, 
                        color="C0", 
                        alpha=0.3, 
                        label="Stdev")

    ax.plot(inter_means, label="Inter-conformation mean", color="C1")
    ax.plot(inter_means+inter_stdevs, color="C1", linestyle="--")
    ax.plot(inter_means-inter_stdevs, color="C1", linestyle="--")

    ax.fill_between(range(0, len(inter_means)), 
                    inter_means+inter_stdevs, 
                    inter_means-inter_stdevs, 
                    color="C1", 
                    alpha=0.3, 
                    label="Stdev")

    ax.ticklabel_format(style='plain')

    ax.set_xlim(0, 25)
    ax.set_xlabel("Residue mask size")

    ax.set_ylabel(u"Score \u00D7 10\u2075 (\u212B)")
    ax.set_ylim(0, 5e+5)
    print(range(0, int(5e+5), 20))
    ax.set_yticks(range(0, int(5e+5), int(2e+4)))
    ax.set_yticklabels(np.arange(0, 50, 2)/10)

    ax.set_title("Score over range of residue mask sizes", fontweight="bold")

    ax.legend()

    plt.show()
    # plt.savefig("score_over_residue_masks.png", dpi=200)
    # plt.savefig("score_over_residue_masks.svg")
