
import pandas as pd
from matplotlib import pyplot as plt
import pathlib
import numpy as np


if __name__ == "__main__":

    path_home = pathlib.Path.home()

    # SUM
    path_home_sum = path_home.joinpath("EMBL-EBI", 
                                    "funclan_work", 
                                    "static-conformer-dataset", 
                                    # "testing_dump", 
                                    "scores")

    path_intra_sum = path_home_sum.joinpath(f"intRA_conformation_score_info_mask_1.csv")
    path_inter_sum = path_home_sum.joinpath(f"intER_conformation_score_info_mask_1.csv")

    df_intra_sum = pd.read_csv(path_intra_sum)
    df_inter_sum = pd.read_csv(path_inter_sum)
    
    # PRODUCT
    path_home_prod = path_home.joinpath("EMBL-EBI", 
                                    "funclan_work", 
                                    "static-conformer-dataset", 
                                    # "testing_dump", 
                                    "scores")

    path_intra_sum = path_home_prod.joinpath(f"intRA_conformation_score_info_mask_1.csv")
    path_inter_sum = path_home_prod.joinpath(f"intER_conformation_score_info_mask_1.csv")

    df_intra_sum = pd.read_csv(path_intra_sum)
    df_inter_sum = pd.read_csv(path_inter_sum)

    # PLotting
    plt.style.use("seaborn-colorblind")
    fig, (ax, ax2, ax3) = plt.subplots(3,1, tight_layout=True, figsize=(7,7))

    ax.grid(alpha=0.3, linestyle="--")

    ax.set_title("Sum: Distribution", fontweight="bold")
    # ax.bar(range(0, len(df_intra_sum)), df_intra_sum["SCORE"])
    # ax.bar(range(0, len(df_inter_sum)), df_inter_sum["SCORE"])

    hist_args = {
        "alpha" : 1, 
        "bins" : 50
    }
    ax.hist(df_inter_sum["SCORE"], **hist_args, label="Inter-conformation")
    ax.hist(df_intra_sum["SCORE"], **hist_args, label="Intra-conformation")

    # Bar chart intra-conformation
    ax2.set_title("Sum: Intra-conformation per PDBe pair")
    df_intra_sum.sort_values(by=["SCORE"], inplace=True)
    ax2.bar(range(0, len(df_intra_sum)),
            df_intra_sum["SCORE"], 
            color="C0"
            )
    ax2.set_xticklabels(df_intra_sum["UNP_ID"], rotation="vertical")

    # Bar chart for inter-conformation
    ax3.set_title("Sum: Inter-conformation per PDBe pair")
    df_inter_sum.sort_values(by=["SCORE"], inplace=True)
    ax3.bar(range(0, len(df_inter_sum)), 
            df_inter_sum["SCORE"],
            color="C1"
            )
    ax3.set_xticklabels(df_inter_sum["UNP_ID"], rotation="vertical")




    ax.legend()

    plt.show()
    # plt.savefig("score_over_residue_masks.png", dpi=200)
    # plt.savefig("score_over_residue_masks.svg")
