

import pathlib
from parsing_tools import *

"""
Converts a set of square matrices stored as N*N dimensional Numpy arrays (in .npy format)
into a single Numpy array, storing it in a desired location. 

Running the benchmark function will perform matrix linearisation for all of the distance
distance matrices within (intra) and between (inter) conformations. 

NB: Include argparse functionality 
"""



if __name__ == "__main__":

    path_home = pathlib.Path.home()
    path_work = path_home.joinpath("EMBL-EBI")
    ####################################################################################
    
    ### MacBook path ###
    # path_work = path_work.joinpath("funclan-work")

    ### Linux path ###
    path_work = path_work.joinpath("funclan_work")

    ####################################################################################
    # Both
    path_work = path_work.joinpath("static-conformer-dataset")#, "testing_dump")
    path_save = path_work.joinpath("master_arrays")

    path_intraconf_matxs = path_work.joinpath("intRA_distance_difference")
    path_interconf_matxs = path_work.joinpath("intER_distance_difference")

    print("Loading intRA-conformation distance matrices:")
    file_names_intra = get_fnames(path_intraconf_matxs)
    intra_matxs = load_martices(file_names_intra, path_intraconf_matxs)

    print("Loading intER-conformation distance matrices:")
    file_names_inter = get_fnames(path_interconf_matxs)
    inter_matxs = load_martices(file_names_inter, path_interconf_matxs)
    
    print('\n', '#'*30, '\n')
    print("   Masking residue windows...")
    print('\n', '#'*30, '\n')

    # Loop over range of residue masks desired
    for i in range(0, 1):   # 26

        # Abstract the code below into a function 
        # ======= Intra ========= #
        print("Masking intRA-conformation matrix residues: Mask window size =", i)
        file_name = f"benchmarking_all_intraconf_dist_diff_res_mask{i}.npy"
        collate_master_vector(intra_matxs, 
                                path=path_save.joinpath(file_name), 
                                res_mask=i, 
                                save=True)

        # ======= Inter ========= #
        print("Masking intER-conformation matrix residues: Mask window size =", i)
        file_name = f"benchmarking_all_interconf_dist_diff_res_mask{i}.npy"
        collate_master_vector(inter_matxs, 
                                path=path_save.joinpath(file_name), 
                                res_mask=i, 
                                save=True)
