from re import S
import pandas as pd
import numpy as np
import os
import pathlib
from ..cluster_conformers.peptide_analysis.tools.linear_algebra import *
from ..cluster_conformers.peptide_analysis.tools.parsing_tools import *
from ..cluster_conformers.peptide_analysis.tools.appearance_utils import *
from sklearn.cluster import AgglomerativeClustering
from matplotlib import pyplot as plt



if __name__ == "__main__":

    path_home = pathlib.Path.home()

    # Loading benchmark dataset
    path_bm = path_home.joinpath(
        "EMBL-EBI",
        "funclan_work",
        "static-conformer-dataset",
        "open_closed_mancur_data",
        "benchmark_monomeric_open_closed_conformers.csv")
    df_bench = pd.read_csv(path_bm)
    # print(df_bench.head())


    # Parsing PDBe structure strings for GESAMT results
    path_gesamt_results = path_home.joinpath(
        "EMBL-EBI",
        "funclan_work",
        "static-conformer-dataset",
        "gesamt_outputs")
    fnames = get_fnames(path_gesamt_results)
    pdbes_dict = {}
    for fname in fnames:
        fname_list = fname.split('_')
        path_file = path_gesamt_results.joinpath(fname)
        gesamt_file = open(path_file).readlines()
        pdbes = []
        num_lines = len(gesamt_file)
        i = 0
        while i < num_lines:
            if gesamt_file[i][:7] == "      S":
                # print(gesamt_file[i][-15:-10])
                pdbes.append(gesamt_file[i][-15:-10])
            i += 1
        key = f"{fname_list[0]}_{fname_list[1][-1]}"
        pdbes_dict[key] = pdbes




    path_gesamt_results = path_home.joinpath(
        "EMBL-EBI",
        "funclan_work",
        "static-conformer-dataset",
        "gesamt_q_score_matrices"
    )

    # Parsing Q-score matrices
    fnames = get_fnames(path_gesamt_results)
    q_score_dict = {}
    for fname in fnames:
        fname_list = fname.split('_')
        path_matx = path_gesamt_results.joinpath(fname)
        # print(path_matx)
        try:
            q_score_matx = np.genfromtxt(fname=path_matx, delimiter="   ", filling_values=np.nan)  # change filling_values as req'd to fill in missing values
            n = q_score_matx.shape[0]
            full_matx = np.full( ( n, n), 1)
            # print(full_matx)
            q_score_matx = full_matx - q_score_matx
            print(q_score_matx)
            if n != 0:
                key = f"{fname_list[0]}_{fname_list[1][-1]}"
                q_score_dict[key] = q_score_matx
        except ValueError:
            print(path_matx, "failed")
    



    # print(q_score_dict.keys())
    # print(len(pdbes_dict))
    # print(len(q_score_dict))

    unps_all = list(pdbes_dict.keys())
    for key in unps_all:
        # print(key)
        if key not in q_score_dict.keys():
            del pdbes_dict[key]

    # print(len(pdbes_dict))
    # print(len(q_score_dict))

    # Output information to send to CSV
    pdbes_all = []
    chains = []
    conformer_id = []
    unps_redundant = []

    # Iterate over all Q-scores stored in UNP files
    unps_all = list(pdbes_dict.keys())
    num_clusters = {}
    for unp in unps_all:

        # print(unp)

        # Code below breaks the dendrogram feature, idiotically...
        print("Generating dendrogram")
        # Get the cutoff
        try:
            model = AgglomerativeClustering(            
                n_clusters=None,
                distance_threshold=0,
                affinity="precomputed",
                linkage="average",
                compute_distances=True
                )
            # print(q_score_dict[unp] )
            model = model.fit( q_score_dict[unp] )
            linkage_matx = make_linkage_matx(model)

            # Cluster based on that cutoff
            cutoff = 0.7
            model = AgglomerativeClustering(            
                n_clusters=None,
                distance_threshold=cutoff*max(linkage_matx[:,2]),
                affinity="precomputed",
                linkage="average",
                compute_distances=True
                )
            model = model.fit( q_score_dict[unp] )

            # print(len(np.unique(model.labels_)))
            num_clusters[unp] = len(np.unique(model.labels_))
        except ValueError:
            pass

        



        # plt.title(f"Agglomerative clustering dendrogram: {unp}", fontweight="bold")
        # print(len(pdbes_dict[unp]))
        # print(len(q_score_dict[unp]))
        # plot_dendrogram(model, labels=pdbes_dict[unp], leaf_rotation=90)  # p=3
        # # plt.xlabel("Number of points in node (or index of point if no parenthesis).")

        # plt.ylabel(u"Score (\u212B)")
        # # plt.xlabel(pdbe_chain_conf_ids)


        # path_save = path_home.joinpath(
        #     "EMBL-EBI",
        #     "funclan_work",
        #     "static-conformer-dataset",
        #     "agglomerative_clustering_dendrograms_rev_q_score_70_pc"
        # )

        # save_fig_dir = path_save.joinpath("png", f"agglomerative_dendrogram_{unp}.png")
        # plt.savefig(save_fig_dir, dpi=200, bbox_inches='tight')

        # save_fig_dir = path_save.joinpath("svg", f"agglomerative_dendrogram_{unp}.svg")
        # plt.savefig(save_fig_dir, bbox_inches='tight')
        # print("Dendrogram saved as", save_fig_dir)
        # # plt.show()

        # plt.close()




        count = 0
        while count < len(pdbes_dict[unp]):
            pdbes_all.append(pdbes_dict[unp][count][:4])
            chains.append(pdbes_dict[unp][count][4])
            conformer_id.append(model.labels_[count])
            unps_redundant.append(unp[:-2])

            count += 1

        # print(pdbe_chain_conf_ids)
        # print(model.labels_)


    



    # # Saving results    
    # count = 0
    # while count < len(num_clusters):

    #     print(unps[count], num_clusters[count])
    #     count += 1

    # path_save_all_conf = path_home.joinpath(
    #     "sum_based_clustering_results_aggregate.csv"
    # )
    
    # df = pd.DataFrame(
    #     {
    #         "UNP_ACC" : unps, 
    #         "NUM_PRED_CONFORMERS" : num_clusters
    #     }
    # )

    # df.to_csv(path_save_all_conf, index=False)



    # Debug info to check cluster information is correctly stored
    count = 0
    while count < len(pdbes_all):

        # print(unps_redundant[count], pdbes_all[count], chains[count], conformer_id[count])
        count += 1
    

    path_save_all_conf = path_home.joinpath(
        "EMBL-EBI",
        "funclan_work",
        "static-conformer-dataset",
        "rev_q_score_cluster_results.csv"
    )
    
    df = pd.DataFrame(
        {
            "UNP_ACC" : unps_redundant, 
            "PDBe_ID" : pdbes_all, 
            "CHAIN_ID" : chains,
            "CONFORMER_ID" : conformer_id
        }
    ) 

    df.to_csv(path_save_all_conf, index=False)










    # count = 0

    # while count < len(num_clusters):

    #     print(unps[count], num_clusters[count])
    #     count += 1

