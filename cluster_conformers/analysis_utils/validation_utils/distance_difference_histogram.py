

from cluster_conformers.peptide_analysis.tools import parsing_tools
from cluster_conformers.peptide_analysis.tools.appearance_utils import progress_bar

import pathlib

import numpy as np

import pandas as pd



if __name__ == "__main__":

    path_distance_difference_matxs = pathlib.Path(
        "/Users",
        "jellaway",
        "EMBL-EBI",
        "funclan_work",
        "contact_map_difference",
        "benchmark_data",
        "all_uniprots",
        "distance_differences"
    )

    print(path_distance_difference_matxs.joinpath("within_conformations"))

    withith_fnames = parsing_tools.get_fnames(
        path_distance_difference_matxs.joinpath("within_conformations")
    )

    between_fnames = parsing_tools.get_fnames(
        path_distance_difference_matxs.joinpath("between_conformations")
    )

    progress = progress_bar(len(withith_fnames) + len(between_fnames))

    out_list = []
    for comparison_type in (withith_fnames, between_fnames):
        for fname in comparison_type:
            if ("2fyc" not in fname) and ("2fyd" not in fname):
                if comparison_type == withith_fnames:
                    dd_matx = np.load(
                        path_distance_difference_matxs.joinpath(
                            "within_conformations",
                            fname),
                            allow_pickle=True
                        )
                else:
                    dd_matx = np.load(
                        path_distance_difference_matxs.joinpath(
                            "between_conformations",
                            fname),
                            allow_pickle=True
                    )

                for i in dd_matx:
                    out_list += list(i)

            progress.update()
    
    out_list = np.asanyarray(out_list)

    np.save("./out_list,npy", out_list)


    