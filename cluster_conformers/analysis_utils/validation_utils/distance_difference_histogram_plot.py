

import numpy as np

from matplotlib import pyplot as plt

if __name__ == "__main__":

    all_dd_elements = np.load(
        "./out_list_all_dd_elements.npy"
    )

    all_dd_elements = all_dd_elements[~np.isnan(all_dd_elements)]

    plt.hist( all_dd_elements, bins=100, range=(0, 10) )

    plt.title("Distance distance frequency distribution")

    plt.xlabel(u"Distance difference (\u212b)")

    plt.ylabel("Frequency")

    plt.show()