import pandas as pd
import numpy as np
import pathlib
from matplotlib import pyplot as plt

import warnings
warnings.filterwarnings("ignore")


if __name__ == "__main__":

    path_home = pathlib.Path.home()

    path_df = path_home.joinpath(
        "EMBL-EBI",
        "funclan_work",
        "static-conformer-dataset",
        "open_closed_mancur_data",
        # "AVoP_vs_benchmark_success_results_perms.csv"
        "AVoP_vs_benchmark_success_results_perms_220607.csv"
    )

    df_avop = pd.read_csv(path_df) 
    
    # print(df_avop.columns)
    """
    Index(['NUM_CONFS', 'NUM_AVoP_PRED_CONFS', 'NUM_PDBes_BENCH', 'NUM_PDBes_AVoP',
       'NUM_PDBes_COMMON', 'NUM_CORRECT_CLUSTERS',
       'NUM_PDBes_IN_CORRECT_CLUSTERS'],
      dtype='object')
    """

    path_df = path_home.joinpath(
        "EMBL-EBI",
        "funclan_work",
        "static-conformer-dataset",
        "open_closed_mancur_data",
        "sum_score_vs_benchmark_success_results_perms_220714_not_flattened.csv"
        # "sum_score_vs_benchmark_success_results_perms_220715_3A_cutoff.csv"
    )

    df_ss = pd.read_csv(path_df)



    # path_df = path_home.joinpath(
    #     "EMBL-EBI",
    #     "funclan_work",
    #     "static-conformer-dataset",
    #     "open_closed_mancur_data",
    #     "rev_qscore_vs_benchmark_success_results_perms.csv"
    # )

    # df_revq = pd.read_csv(path_df)




    # df_avop = df_avop[df_avop["NUM_CONFS"] > 2]
    # df_ss = df_ss[df_ss["NUM_CONFS"] > 2]

    # Mean for all conformation predictions
    print('#'*5, "Percentage correct conformers", '#'*5)
    df_avop["PC_SUCCESS"] = df_avop["NUM_CORRECT_CLUSTERS"]/df_avop["NUM_CONFS"]
    print("Aggregate Views of Proteins success rate:", round(np.mean(df_avop["PC_SUCCESS"])*100, 3), '%')

    df_ss["PC_SUCCESS"] = df_ss["NUM_CORRECT_CLUSTERS"]/df_ss["NUM_CONFS"]
    print("Sum-based score success rate:", round(np.mean(df_ss["PC_SUCCESS"])*100, 3), '%')
    print("Percentage improvement =", round((np.mean(df_ss["PC_SUCCESS"]) - np.mean(df_avop["PC_SUCCESS"]))*100 / np.mean(df_avop["PC_SUCCESS"]), 3), '%')
    print()

    # df_revq["PC_SUCCESS"] = df_revq["NUM_CORRECT_CLUSTERS"]/df_revq["NUM_CONFS"]
    # print("Reverse Q-score success rate:", round(np.mean(df_revq["PC_SUCCESS"])*100, 3), '%')
    # print("Percentage improvement =", round((np.mean(df_ss["PC_SUCCESS"]) - np.mean(df_revq["PC_SUCCESS"]))*100 / np.mean(df_revq["PC_SUCCESS"]), 3), '%')






    df_ss_no_doubles = df_ss[df_ss["NUM_PDBes_BENCH"] > 2]
    df_avop_no_doubles = df_avop[df_avop["NUM_PDBes_BENCH"] > 2 ]
    # df_revq_no_doubles = df_revq[df_revq["NUM_PDBes_BENCH"] > 2 ]
    print()
    print("Omitting single open-closed entries")
    # df_ss_no_doubles["PC_SUCCESS"] = df_ss_no_doubles["NUM_CORRECT_CLUSTERS"]/df_ss_no_doubles["NUM_CONFS"]
    print("Aggregate Views of Proteins success rate:", round(np.mean(df_avop_no_doubles["PC_SUCCESS"])*100, 3), '%')
    print("Sum-based score success rate:", round(np.mean(df_ss_no_doubles["PC_SUCCESS"])*100, 3), '%')
    print("Percentage improvement =", round((np.mean(df_ss_no_doubles["PC_SUCCESS"]) - np.mean(df_avop_no_doubles["PC_SUCCESS"]))*100 / np.mean(df_avop_no_doubles["PC_SUCCESS"]), 3), '%')

    print()
    # print("Reverse Q-score success rate:", round(np.mean(df_revq_no_doubles["PC_SUCCESS"])*100, 3), '%')
    # print("Percentage improvement =", round((np.mean(df_ss_no_doubles["PC_SUCCESS"]) - np.mean(df_revq_no_doubles["PC_SUCCESS"]))*100 / np.mean(df_revq_no_doubles["PC_SUCCESS"]), 3), '%')


    # print('\n', '#'*20)

    # df_avop_no_doubles["num_successful_peptides"] = df_avop_no_doubles["NUM_PDBes_IN_CORRECT_CLUSTERS"]/df_avop_no_doubles["NUM_PDBes_COMMON"]

    # df_ss_no_doubles["num_successful_peptides"] = df_ss_no_doubles["NUM_PDBes_IN_CORRECT_CLUSTERS"]/df_ss_no_doubles["NUM_PDBes_COMMON"]
    # print()
    # print("Aggregate Views of Proteins success rate:", round(np.mean(df_avop_no_doubles["num_successful_peptides"])*100, 3), '%')
    # print("Sum-based score success rate:", round(np.mean(df_ss_no_doubles["num_successful_peptides"])*100, 3), '%')
    # print("Percentage improvement =", round((np.mean(df_ss_no_doubles["num_successful_peptides"]) - np.mean(df_avop_no_doubles["num_successful_peptides"]))*100 / np.mean(df_avop_no_doubles["num_successful_peptides"]), 3), '%')
    
    # print()
    # print(np.sum(df_ss["NUM_CORRECT_CLUSTERS"]))
    # print()

    print('\n', '#'*20, '\n')


    print('#'*5, "Percentage correct chains", '#'*5)
    df_avop["PC_SUCCESS"] = df_avop["NUM_PDBes_IN_CORRECT_CLUSTERS"]/df_avop["NUM_PDBes_COMMON"]
    print("Aggregate Views of Proteins success rate:", round(np.mean(df_avop["PC_SUCCESS"])*100, 3), '%')

    df_ss["PC_SUCCESS"] = df_ss["NUM_PDBes_IN_CORRECT_CLUSTERS"]/df_ss["NUM_PDBes_COMMON"]
    print("Sum-based score success rate:", round(np.mean(df_ss["PC_SUCCESS"])*100, 3), '%')
    print("Percentage improvement =", round((np.mean(df_ss["PC_SUCCESS"]) - np.mean(df_avop["PC_SUCCESS"]))*100 / np.mean(df_avop["PC_SUCCESS"]), 3), '%')
    print()

    # df_revq["PC_SUCCESS"] = df_revq["NUM_PDBes_IN_CORRECT_CLUSTERS"]/df_revq["NUM_PDBes_COMMON"]
    # print("Reverse Q-score success rate:", round(np.mean(df_revq["PC_SUCCESS"])*100, 3), '%')
    # print("Percentage improvement =", round((np.mean(df_ss["PC_SUCCESS"]) - np.mean(df_revq["PC_SUCCESS"]))*100 / np.mean(df_revq["PC_SUCCESS"]), 3), '%')




    df_ss_no_doubles = df_ss[df_ss["NUM_PDBes_BENCH"] > 2]
    df_avop_no_doubles = df_avop[df_avop["NUM_PDBes_BENCH"] > 2 ]
    # df_revq_no_doubles = df_revq[df_revq["NUM_PDBes_BENCH"] > 2 ]
    print()
    print("Omitting single open-closed entries")
    # df_ss_no_doubles["PC_SUCCESS"] = df_ss_no_doubles["NUM_CORRECT_CLUSTERS"]/df_ss_no_doubles["NUM_CONFS"]
    print("Aggregate Views of Proteins success rate:", round(np.mean(df_avop_no_doubles["PC_SUCCESS"])*100, 3), '%')
    print("Sum-based score success rate:", round(np.mean(df_ss_no_doubles["PC_SUCCESS"])*100, 3), '%')
    print("Percentage improvement =", round((np.mean(df_ss_no_doubles["PC_SUCCESS"]) - np.mean(df_avop_no_doubles["PC_SUCCESS"]))*100 / np.mean(df_avop_no_doubles["PC_SUCCESS"]), 3), '%')

    print()
    # print("Reverse Q-score success rate:", round(np.mean(df_revq_no_doubles["PC_SUCCESS"])*100, 3), '%')
    # print("Percentage improvement =", round((np.mean(df_ss_no_doubles["PC_SUCCESS"]) - np.mean(df_revq_no_doubles["PC_SUCCESS"]))*100 / np.mean(df_revq_no_doubles["PC_SUCCESS"]), 3), '%')


    # print('\n', '#'*20)

    # df_avop_no_doubles["num_successful_peptides"] = df_avop_no_doubles["NUM_PDBes_IN_CORRECT_CLUSTERS"]/df_avop_no_doubles["NUM_PDBes_COMMON"]

    # df_ss_no_doubles["num_successful_peptides"] = df_ss_no_doubles["NUM_PDBes_IN_CORRECT_CLUSTERS"]/df_ss_no_doubles["NUM_PDBes_COMMON"]
    # print()
    # print("Aggregate Views of Proteins success rate:", round(np.mean(df_avop_no_doubles["num_successful_peptides"])*100, 3), '%')
    # print("Sum-based score success rate:", round(np.mean(df_ss_no_doubles["num_successful_peptides"])*100, 3), '%')
    # print("Percentage improvement =", round((np.mean(df_ss_no_doubles["num_successful_peptides"]) - np.mean(df_avop_no_doubles["num_successful_peptides"]))*100 / np.mean(df_avop_no_doubles["num_successful_peptides"]), 3), '%')
    
    # print()
    # print(np.sum(df_ss["NUM_CORRECT_CLUSTERS"]))
    # print()



    # success_cutoff = 0.5
    # failed_ss = df_ss[df_ss["PC_SUCCESS"] < success_cutoff]
    # failed_avop = df_avop[df_avop["PC_SUCCESS"] < success_cutoff]

    # print(failed_ss.shape[0])
    # print(failed_avop.shape[0])

    # print(failed_ss["UNP_ACC"])

    # overlap = pd.merge(failed_ss, failed_avop, how="inner", on=["UNP_ACC"])
    # print(overlap)

    # print(list(overlap["UNP_ACC"]))

    


    # print("Mean pc of correct predictions (AVoP):", np.mean(df["PC_AVoP_SUCCESS"]))
    # print("Mean pc of correct predictions (AVoP):", np.mean(df["PC_SUMSCORE_SUCCESS"]))
    # print("Percentage improvement = ", (np.mean(df["PC_SUMSCORE_SUCCESS"]) - np.mean(df["PC_AVoP_SUCCESS"])) / np.mean(df["PC_AVoP_SUCCESS"]))

    # two_hits_dropped = df[df["NUM_PDBes_BENCH"] > 2]

    # print(df.shape[0])
    # print(two_hits_dropped.shape[0])

    # # Mean for conformation predictions with > 2 structures
    # print("Mean pc of correct predictions (AVoP):", np.mean(two_hits_dropped["PC_AVoP_SUCCESS"]))
    # print("Mean pc of correct predictions (AVoP):", np.mean(two_hits_dropped["PC_SUMSCORE_SUCCESS"]))
    # print("Percentage improvement = ", (np.mean(two_hits_dropped["PC_SUMSCORE_SUCCESS"]) - np.mean(two_hits_dropped["PC_AVoP_SUCCESS"])) / np.mean(two_hits_dropped["PC_AVoP_SUCCESS"]))


    # # Data visualisations
    # plt.style.use("seaborn-colorblind")

    # plt.scatter(df["NUM_PDBes_BENCH"], df["PC_SUMSCORE_SUCCESS"], label="Sum score")
    # plt.scatter(df["NUM_PDBes_AVoP"][df["NUM_PDBes_AVoP"] <= df["NUM_PDBes_BENCH"].max() ], df["PC_AVoP_SUCCESS"][df["NUM_PDBes_AVoP"] <= df["NUM_PDBes_BENCH"].max() ], label="rev. Q score", marker="x")
    # plt.legend()

    # plt.show()

    # """
    # Index(['NUM_CONFS', 'NUM_AVoP_PRED_CONFS', 'NUM_SUM_SCORE_PRED_CONFS',
    #    'NUM_PDBes_BENCH', 'NUM_PDBes_AVoP', 'NUM_PDBes_COMMON',
    #    'NUM_AVoP_CORRECT_CLUSTERS', 'NUM_SUMSCORE_CORRECT_CLUSTERS',
    #    'NUM_PDBes_IN_CORRECT_AVoP_CLUSTERS',
    #    'NUM_PDBes_IN_CORRECT_SUM_SCORE_CLUSTERS', 'PC_AVoP_SUCCESS',
    #    'PC_SUMSCORE_SUCCESS', 'NUM_PERMs_BENCH', 'NUM_PERMs_COMMON',
    #    'PROB_CORRECT_BENCH', 'SIGNIFICANT'],
    #   dtype='object')
    # """

    # # print( df.head() )
    
