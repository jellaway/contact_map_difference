


from cluster_conformers.utils import parsing_utils, io_utils



if __name__ == "__main__":

    mmcif_block = io_utils.load_mmcif(
        "benchmark_data/examples/O34926/O34926_updated_mmcif/3nc7_updated.cif"
    )

    for i in range(0, 500):
        mmcif_vars = parsing_utils.parse_mmcif(mmcif_block, 'A')

