import pandas as pd
import numpy as np
import pathlib




if __name__ == "__main__":

    path_home = pathlib.Path.home()

    # Load AVoP results
    path_avop = path_home.joinpath(
        "EMBL-EBI",
        "funclan_work",
        "static-conformer-dataset",
        "open_closed_mancur_data",
        "AVoP_vs_benchmark_success_results_perms3.csv"
    )
    df_avop = pd.read_csv(path_avop)

    # # Load benchmark results
    # path_bench = path_home.joinpath(
    #     "EMBL-EBI",
    #     "funclan_work",
    #     "static-conformer-dataset",
    #     "open_closed_mancur_data",
    #     # "benchmark_monomeric_open_closed_conformers.csv"
    #     "benchmarking_monomeric_open_closed_conformers.csv"
    # )
    # df_bench = pd.read_csv(path_bench)

    probs = []

    row_counter = 0

    while row_counter < df_avop.shape[0]:

        num_permutations_bench = df_avop["NUM_PERMs_BENCH"].iloc[row_counter]
        num_correct_permutations = df_avop["NUM_CONFS"].iloc[row_counter]
        num_correct_preds = df_avop["NUM_CORRECT_CLUSTERS"].iloc[row_counter]

        while num_correct_preds > 0:

            prob_correct = 0

            num_correct_preds -= 1




        row_counter += 1




    print(df_avop.head())








    # Reference for adding info to DF at end
    # conformer_ids_avop = list(df_avop["structural_cluster_id"].str[-1])
    # conformer_ids_avop = pd.DataFrame(conformer_ids_avop, columns=["CONFORMER_ID"])
    # df_avop = pd.concat([df_avop, conformer_ids_avop], axis=1)





    pass