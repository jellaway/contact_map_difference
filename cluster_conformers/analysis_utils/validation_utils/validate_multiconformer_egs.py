import pandas as pd
import numpy as np
import pathlib
from matplotlib import pyplot as plt
import warnings
warnings.filterwarnings("ignore")


if __name__ == "__main__":

    path_home = pathlib.Path.home()

    path_df = path_home.joinpath(
        "EMBL-EBI",
        "funclan_work",
        "static-conformer-dataset",
        "open_closed_mancur_data",
        "AVoP_vs_benchmark_success_results_perms_220607.csv"
    )

    df_avop = pd.read_csv(path_df) 
    
    """
    Index(['NUM_CONFS', 'NUM_AVoP_PRED_CONFS', 'NUM_PDBes_BENCH', 'NUM_PDBes_AVoP',
       'NUM_PDBes_COMMON', 'NUM_CORRECT_CLUSTERS',
       'NUM_PDBes_IN_CORRECT_CLUSTERS'],
      dtype='object')
    """

    path_df = path_home.joinpath(
        "EMBL-EBI",
        "funclan_work",
        "static-conformer-dataset",
        "open_closed_mancur_data",
        "sum_score_vs_benchmark_success_results_perms_220608.csv"
    )

    df_ss = pd.read_csv(path_df)





    # Mean for all conformation predictions
    print('#'*5, "Percentage correct conformers", '#'*5)
    df_avop["PC_SUCCESS"] = df_avop["NUM_CORRECT_CLUSTERS"]/df_avop["NUM_CONFS"]
    print("Aggregate Views of Proteins success rate:", round(np.mean(df_avop["PC_SUCCESS"])*100, 3), '%')

    df_ss["PC_SUCCESS"] = df_ss["NUM_CORRECT_CLUSTERS"]/df_ss["NUM_CONFS"]
    print("Sum-based score success rate:", round(np.mean(df_ss["PC_SUCCESS"])*100, 3), '%')
    print("Percentage improvement =", round((np.mean(df_ss["PC_SUCCESS"]) - np.mean(df_avop["PC_SUCCESS"]))*100 / np.mean(df_avop["PC_SUCCESS"]), 3), '%')
    print()





    df_ss_no_doubles = df_ss[df_ss["NUM_PDBes_BENCH"] > 2]
    df_avop_no_doubles = df_avop[df_avop["NUM_PDBes_BENCH"] > 2 ]
    print()
    print("Omitting single open-closed entries")
    print("Aggregate Views of Proteins success rate:", round(np.mean(df_avop_no_doubles["PC_SUCCESS"])*100, 3), '%')
    print("Sum-based score success rate:", round(np.mean(df_ss_no_doubles["PC_SUCCESS"])*100, 3), '%')
    print("Percentage improvement =", round((np.mean(df_ss_no_doubles["PC_SUCCESS"]) - np.mean(df_avop_no_doubles["PC_SUCCESS"]))*100 / np.mean(df_avop_no_doubles["PC_SUCCESS"]), 3), '%')

    print()


    print('\n', '#'*20, '\n')


    print('#'*5, "Percentage correct chains", '#'*5)
    df_avop["PC_SUCCESS"] = df_avop["NUM_PDBes_IN_CORRECT_CLUSTERS"]/df_avop["NUM_PDBes_COMMON"]
    print("Aggregate Views of Proteins success rate:", round(np.mean(df_avop["PC_SUCCESS"])*100, 3), '%')

    df_ss["PC_SUCCESS"] = df_ss["NUM_PDBes_IN_CORRECT_CLUSTERS"]/df_ss["NUM_PDBes_COMMON"]
    print("Sum-based score success rate:", round(np.mean(df_ss["PC_SUCCESS"])*100, 3), '%')
    print("Percentage improvement =", round((np.mean(df_ss["PC_SUCCESS"]) - np.mean(df_avop["PC_SUCCESS"]))*100 / np.mean(df_avop["PC_SUCCESS"]), 3), '%')
    print()


    df_ss_no_doubles = df_ss[df_ss["NUM_PDBes_BENCH"] > 2]
    df_avop_no_doubles = df_avop[df_avop["NUM_PDBes_BENCH"] > 2 ]
    print()
    print("Omitting single open-closed entries")
    print("Aggregate Views of Proteins success rate:", round(np.mean(df_avop_no_doubles["PC_SUCCESS"])*100, 3), '%')
    print("Sum-based score success rate:", round(np.mean(df_ss_no_doubles["PC_SUCCESS"])*100, 3), '%')
    print("Percentage improvement =", round((np.mean(df_ss_no_doubles["PC_SUCCESS"]) - np.mean(df_avop_no_doubles["PC_SUCCESS"]))*100 / np.mean(df_avop_no_doubles["PC_SUCCESS"]), 3), '%')

    print()




    print('\n', '#'*20, '\n')
    print('\n', '#'*20, '\n')
    print('\n', '#'*20, '\n')
    print('\n', '#'*20, '\n')


    df_avop = df_avop[df_avop["NUM_CONFS"] > 2]
    df_ss = df_ss[df_ss["NUM_CONFS"] > 2]

    print(len(df_ss["UNP_ACC"].unique()))

    # Mean for all conformation predictions
    print('#'*5, "Percentage correct conformers", '#'*5)
    df_avop["PC_SUCCESS"] = df_avop["NUM_CORRECT_CLUSTERS"]/df_avop["NUM_CONFS"]
    print("Aggregate Views of Proteins success rate:", round(np.mean(df_avop["PC_SUCCESS"])*100, 3), '%')

    df_ss["PC_SUCCESS"] = df_ss["NUM_CORRECT_CLUSTERS"]/df_ss["NUM_CONFS"]
    print("Sum-based score success rate:", round(np.mean(df_ss["PC_SUCCESS"])*100, 3), '%')
    print("Percentage improvement =", round((np.mean(df_ss["PC_SUCCESS"]) - np.mean(df_avop["PC_SUCCESS"]))*100 / np.mean(df_avop["PC_SUCCESS"]), 3), '%')
    print()





    df_ss_no_doubles = df_ss[df_ss["NUM_PDBes_BENCH"] > 2]
    df_avop_no_doubles = df_avop[df_avop["NUM_PDBes_BENCH"] > 2 ]
    print()
    print("Omitting single open-closed entries")
    print("Aggregate Views of Proteins success rate:", round(np.mean(df_avop_no_doubles["PC_SUCCESS"])*100, 3), '%')
    print("Sum-based score success rate:", round(np.mean(df_ss_no_doubles["PC_SUCCESS"])*100, 3), '%')
    print("Percentage improvement =", round((np.mean(df_ss_no_doubles["PC_SUCCESS"]) - np.mean(df_avop_no_doubles["PC_SUCCESS"]))*100 / np.mean(df_avop_no_doubles["PC_SUCCESS"]), 3), '%')

    print()


    print('\n', '#'*20, '\n')


    print('#'*5, "Percentage correct chains", '#'*5)
    df_avop["PC_SUCCESS"] = df_avop["NUM_PDBes_IN_CORRECT_CLUSTERS"]/df_avop["NUM_PDBes_COMMON"]
    print("Aggregate Views of Proteins success rate:", round(np.mean(df_avop["PC_SUCCESS"])*100, 3), '%')

    df_ss["PC_SUCCESS"] = df_ss["NUM_PDBes_IN_CORRECT_CLUSTERS"]/df_ss["NUM_PDBes_COMMON"]
    print("Sum-based score success rate:", round(np.mean(df_ss["PC_SUCCESS"])*100, 3), '%')
    print("Percentage improvement =", round((np.mean(df_ss["PC_SUCCESS"]) - np.mean(df_avop["PC_SUCCESS"]))*100 / np.mean(df_avop["PC_SUCCESS"]), 3), '%')
    print()


    df_ss_no_doubles = df_ss[df_ss["NUM_PDBes_BENCH"] > 2]
    df_avop_no_doubles = df_avop[df_avop["NUM_PDBes_BENCH"] > 2 ]
    print()
    print("Omitting single open-closed entries")
    print("Aggregate Views of Proteins success rate:", round(np.mean(df_avop_no_doubles["PC_SUCCESS"])*100, 3), '%')
    print("Sum-based score success rate:", round(np.mean(df_ss_no_doubles["PC_SUCCESS"])*100, 3), '%')
    print("Percentage improvement =", round((np.mean(df_ss_no_doubles["PC_SUCCESS"]) - np.mean(df_avop_no_doubles["PC_SUCCESS"]))*100 / np.mean(df_avop_no_doubles["PC_SUCCESS"]), 3), '%')

    print()





    
