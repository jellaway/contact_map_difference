import pandas as pd
import numpy as np
import pathlib
import os
from ..tools.parsing_tools import get_fnames
# from linear_algebra import stirling2k

def generate_cluster_paths():
    # Initialise directory structure
    path_home = pathlib.Path.home()

    path_home = path_home.joinpath(
        "EMBL-EBI",
        "funclan_work"
    )

    path_bench = path_home.joinpath(
        "static-conformer-dataset",
        "open_closed_mancur_data",
        "benchmarking_monomeric_open_closed_conformers.csv"
    )

    df = pd.read_csv(path_bench)

    cp_str = "cp -r ./{"

    for unp in df["UNP_ACC"].unique():
        print(unp)

        file_path = f"{unp[0]}/{unp}/{unp}_clusters.csv"

        cp_str += file_path
        cp_str += ','

    cp_str = cp_str[:-1]
    cp_str += "} /nfs/msd/work2/jellaway/data-dump/"

    print(cp_str)

    return cp_str



def collate_avop_results():

    path_home = pathlib.Path.home()

    path_avop = path_home.joinpath(
        "EMBL-EBI",
        "funclan_work",
        "static-conformer-dataset",
        "AVoP_cluster_results"
    )
    path_save = path_home.joinpath(
        "EMBL-EBI",
        "funclan_work",
        "static-conformer-dataset"
    )

    avop_cluster_files = get_fnames(path_avop)

    print(avop_cluster_files)


    avop_df = pd.DataFrame(columns=[
        "pdb_id",
        "auth_asym_id",
        "struct_asym_id",
        "entity_id",
        "structural_cluster_id",
        "sequence_segment_id",
        "segment_begin",
        "segment_end",
        "segment_sequence_ranges",
        "uniprot_accession"
        ])

    for file in avop_cluster_files:

        tmp_df = pd.read_csv(path_avop.joinpath(file))

        avop_df = avop_df.append(tmp_df, ignore_index=True)

        print(len(avop_df))

    avop_df.to_csv(path_save.joinpath("avop_results.csv"), index=False)







if __name__ == "__main__":

    """
    Assesses the accuracy of the current Aggregate Views of Proteins (AVoP) 
    conformational clustering pipeline. 

    TO DO:
        Implement probability of correct column using Stirling distribution
    """

    # Uncomment to create a single DataFrame containing all of the AVoP information
    # collate_avop_results()

    path_home = pathlib.Path.home()

    # Load AVoP results
    path_avop = path_home.joinpath(
        "EMBL-EBI",
        "funclan_work",
        "static-conformer-dataset",
        "avop_results.csv"
    )
    df_avop = pd.read_csv(path_avop)

    # Load benchmark results
    path_bench = path_home.joinpath(
        "EMBL-EBI",
        "funclan_work",
        "static-conformer-dataset",
        "open_closed_mancur_data",
        "benchmark_monomeric_open_closed_conformers.csv"
        # "benchmarking_monomeric_open_closed_conformers.csv"
    )
    df_bench = pd.read_csv(path_bench)

    

    """
    Parsing clustering information from the current AVoP page
    """
    df_avop.sort_values(by=["uniprot_accession"], inplace=True)

    # Extract AVoP conformation IDs and add to AVoP DataFrame
    conformer_ids_avop = list(df_avop["structural_cluster_id"].str[-1])
    conformer_ids_avop = pd.DataFrame(conformer_ids_avop, columns=["CONFORMER_ID"])
    df_avop = pd.concat([df_avop, conformer_ids_avop], axis=1)
    # print(df_avop["CONFORMER_ID"][df_avop["CONFORMER_ID"] == np.nan])
    df_avop["CONFORMER_ID"] = df_avop["CONFORMER_ID"].astype(int)

    # Rename AVoP columns to match benchmark schema
    df_avop.rename(columns = {
        "uniprot_accession" : "UNP_ACC", 
        "segment_begin" : "UNP_START",
        "segment_end" : "UNP_END",
        "pdb_id" : "PDBe_ID",
        "auth_asym_id" : "CHAIN_ID"
        }, 
        inplace = True)

    # Remove unused columns
    drop_cols = [
        "struct_asym_id", 
        "entity_id", 
        "structural_cluster_id", 
        "sequence_segment_id",
        "segment_sequence_ranges"
    ]
    df_avop.drop(drop_cols, axis=1, inplace=True)

    """
    Comparing the AVoP results against the benchmark dataset
    """
    # Initialise lists to fill with useful comparison information
    unp_list = []                   # UniProt accessions
    num_confs_list = []             # Number of conformers in benchmark dataset
    num_avop_pred_confs_list = []   # Number of conformers predicted by AVoP
    num_pdbes_bench = []            # Number of structures for conformation (benchmark)
    num_pdbes_avop = []             # Number of structures for conformation (AVoP)
    num_corrects = []               # Number of correctly predicted conformations
    num_correct_structs = []        # Number of structures assigned to correct conformation
    num_common_pdbes = []           # Number of PDBe shared benchmark-AVoP hits
    num_permutations_avop = []
    num_permutations_bench = []
    num_permutations_common = []

    # Performing the comparison
    for unp in df_avop["UNP_ACC"].unique():

        # Append UniProt accessions
        unp_list.append(unp)

        # Selecting rows corresponding to current UniProt accession only
        df_bench_unp = df_bench[df_bench["UNP_ACC"] == unp]
        df_avop_unp = df_avop[df_avop["UNP_ACC"] == unp]

        # print(df_avop_unp)

        # Ensuring only overlapping UniProt segments are compared
        # This step is not needed here as only common PDBe entries are selected 
        # df_avop_unp = df_avop_unp[df_avop_unp.apply(lambda x: any((x["UNP_END"] >= df_bench_unp["UNP_START"]) |
        #                          (x["UNP_END"] >= df_bench_unp["UNP_START"])), axis=1)]

        # print("Benchmark information")
        # print(df_bench_unp)
        # print(len(df_bench_unp["UNP_START"].unique()))
        # print(len(df_bench_unp["UNP_END"].unique()))
        # print("AVoP information")
        # print(df_avop_unp)
        # print(len(df_avop_unp["UNP_START"].unique()))
        # print(len(df_avop_unp["UNP_END"].unique()))

        # Get number of conformers for UniProt 
        num_confs = df_bench_unp["CONFORMER_ID"].max()
        num_confs_list.append(num_confs)

        num_avop_pred_confs = df_avop_unp["CONFORMER_ID"].max()
        num_avop_pred_confs_list.append(num_avop_pred_confs)

        # Find number of PDBes per UniProt
        num_pdbes_bench.append(df_bench_unp.shape[0])
        num_pdbes_avop.append(df_avop_unp.shape[0])

        # Find number of permutations for benchmark
        # permutations_bench_all = 0
        # k = df_bench_unp.shape[0]
        # while k > 0 and k<22:   # Condition here will need removing later
        #     permutations_bench = stirling2k(df_bench_unp.shape[0], k)
        #     permutations_bench_all += permutations_bench
        #     k -= 1
        # num_permutations_bench.append(permutations_bench_all)

        # permutations_avop = stirling2k(df_avop_unp.shape[0], num_confs)
        # num_permutations_avop.append(permutations_avop)

        # Finding the number of currectly clustered conformations
        df_bench_short = df_bench_unp[df_bench_unp.set_index(["PDBe_ID","CHAIN_ID"]).index.isin(df_avop_unp.set_index(["PDBe_ID","CHAIN_ID"]).index)]
        df_avop_short = df_avop_unp[df_avop_unp.set_index(["PDBe_ID","CHAIN_ID"]).index.isin(df_bench_unp.set_index(["PDBe_ID","CHAIN_ID"]).index)]

        # print(unp)
        # print(df_bench_short["CONFORMER_ID"])
        # print()
        # print(df_avop_short["CONFORMER_ID"])
        
        # if df_bench_short.shape[0] == df_bench_unp.shape[0]:
        #     print(True)
        # else:
        #     print(df_bench_unp)
        #     print(df_bench_short)
        #     print(False)
        # print(df_avop_short)

        num_common_pdbes.append(df_bench_short.shape[0])    # Either bench or AVoP

        # Find number of permutations for common PDBe structures
        # permutations_common_all = 0
        # k = df_bench_short.shape[0]
        # while k > 0 and k<22:
        #     permutations_common = stirling2k(df_avop_short.shape[0], k)
        #     permutations_common_all += permutations_common
        #     k -= 1
        # num_permutations_common.append(permutations_common_all)

        # print(permutations_bench, permutations_avop, permutations_common)
        # print(unp)
        # print(permutations_bench_all, permutations_common_all)

        num_common_confs = df_bench_short["CONFORMER_ID"].max()
        num_common_avop_pred_confs = df_avop_short["CONFORMER_ID"].max()
        # print(num_common_confs)
        # print(num_common_avop_pred_confs)

        num_correct = 0     # Keep track of number correct clusters
        num_correct_struct = 0

        # Could abstract the contents of these conditions into a function
        if num_common_confs >= num_common_avop_pred_confs:
            
            # if unp == "P9WNX1":
            #     print("True"*100)
            
            count = 1
            while count <= num_common_avop_pred_confs:

                pdbes_in_conf_avop = df_avop_short[df_avop_short["CONFORMER_ID"] == count]
                # print(pdbes_in_conf_avop)

                intersect_bench = pd.merge(
                    pdbes_in_conf_avop, 
                    df_bench_short, 
                    how="inner", 
                    on=["PDBe_ID", "CHAIN_ID"]
                    )

                if unp == "P9WNX1":

                    print(intersect_bench[["PDBe_ID", "CHAIN_ID", "CONFORMER_ID_y"]])
                    print(pdbes_in_conf_avop[["PDBe_ID", "CHAIN_ID", "CONFORMER_ID"]])
                    print(df_bench_short[["PDBe_ID", "CHAIN_ID", "CONFORMER_ID"]])
                    # print(intersect_bench[])

                # print(intersect_bench)

                if intersect_bench.empty:
                    pass

                if len(intersect_bench["CONFORMER_ID_y"].unique()) == 1:
                    num_correct += 1
                    num_correct_struct += intersect_bench.shape[0]
                else:
                    # print(len(intersect_bench["CONFORMER_ID_y"].unique()))
                    # print(intersect_bench.shape[0])
                    pass

                count += 1

        else:

            count = 1
            while count <= num_common_confs:

                pdbes_in_conf_bench = df_bench_short[df_bench_short["CONFORMER_ID"] == count]

                intersect_bench = pd.merge(
                    pdbes_in_conf_bench, 
                    df_avop_short, 
                    how="inner", 
                    on=["PDBe_ID", "CHAIN_ID"]
                    )
                if intersect_bench.empty:
                    # print(unp, "EMPTY")
                    pass

                if len(intersect_bench["CONFORMER_ID_y"].unique()) == 1:
                    # print(len(intersect_bench["CONFORMER_ID_y"].unique()))
                    num_correct += 1
                    num_correct_struct += intersect_bench.shape[0]
                    
                    
                else:
                    # print(len(intersect_bench["CONFORMER_ID_y"].unique()))
                    # print(intersect_bench.shape[0])
                    pass

                count += 1

        num_corrects.append(num_correct)
        num_correct_structs.append(num_correct_struct)

        # print(num_correct)
        # print('='*10)
        
    df_compare = pd.DataFrame(
        {
            "UNP_ACC" : unp_list,
            "NUM_CONFS" : num_confs_list, 
            "NUM_AVoP_PRED_CONFS" : num_avop_pred_confs_list,
            "NUM_PDBes_BENCH" : num_pdbes_bench,
            "NUM_PDBes_AVoP" : num_pdbes_avop,
            "NUM_PDBes_COMMON" : num_common_pdbes,
            "NUM_CORRECT_CLUSTERS" : num_corrects,
            "NUM_PDBes_IN_CORRECT_CLUSTERS" : num_correct_structs#,
            # "NUM_PERMs_AVoP" : num_permutations_avop,
            # "NUM_PERMs_BENCH" : num_permutations_bench,
            # "NUM_PERMs_COMMON" : num_permutations_common
        }
    )

    print(np.sum(df_compare["NUM_PDBes_IN_CORRECT_CLUSTERS"]))

    path_save = path_home.joinpath(
        "EMBL-EBI",
        "funclan_work",
        "static-conformer-dataset", 
        "open_closed_mancur_data"
    )

    # df_compare.to_csv(path_save.joinpath("AVoP_vs_benchmark_success_results_perms_220607.csv"), index=False)



        









        
    





    


