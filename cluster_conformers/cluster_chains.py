"""
Performs agglomerative clustering on the sum-based scores generated from CA distance
difference matrices.

Script to plot a dendrogram of the hierarchical clustering results. Can either load the
results from a pre-saved CSV of the clustering output from cluster.py or work on the
benchmark dataset.

Functions included herein to generate a swarm plots from a given dir of pre-generated
distance difference maps.
"""


from pathlib import Path

# Standard package imports
from typing import Iterable

import seaborn as sns
from matplotlib import pyplot as plt
from matplotlib.axes import Axes
from matplotlib.figure import Figure
from numpy import asarray, column_stack, maximum, ndarray, zeros
from scipy.cluster.hierarchy import dendrogram
from sklearn.cluster import AgglomerativeClustering

# Custom module imports
from cluster_conformers import distance_differences
from cluster_conformers.utils import io_utils, linear_algebra_utils

# To suppress Matplotlib debug messages
# import os
# os.environ["QT_LOGGING_RULES"] = "qt5ct.debug=false"
# import warnings
# warnings.filterwarnings("ignore")


def build_inputs(
    unp: str,
    pdbe_chain_ids: "list[str]",
    unp_res_ids: "dict[str, Path]",
    path_save: Path,
    ca_matxs: "dict[str, Path]",
) -> "tuple[ ndarray[any, float], ndarray[any, str] ]":
    """
    Constructs the requisite datastructures for parsing into the cluster_agglomerative()
    function.

    :param unp: UniProt accession
    :type unp: str
    :param pdbe_chain_ids: List of non-redundant PDB-chain IDs for all parsed chains.
    :type pdbe_chain_ids: list[str]
    :param unp_res_ids: Dictionary of locations to serialised UniProt residue IDs per
        chain.
    :type unp_res_ids: dict[str, pathlib.Path]
    :param path_save: Path to save calculated CA distance-difference matrices.
    :type path_save: pathlib.Path
    :param ca_matxs: Dictionary of locations to saved, serialised CA distance matrices.
        Keys are PDB-chain ID strings.
    :type ca_matxs: dict[str, pathlib.Path]
    :return: Score matrix and label matrix, corresponding to the chain-chain comparisons
        in the score matrix.
    :rtype: tuple[ ndarray[any, float], ndarray[any, str] ]
    """

    score_matx = []  # Matrix of scores from chain-chain comparisons.
    label_matx = []  # Matrix of labels corresponding to score_matx.

    # num_chains = len(pdbe_chain_ids)
    # progress = appearance_utils.ProgressBar(num_chains**2)

    path_root_serial = path_save.joinpath("serialised_dd_matxs")
    path_root_serial.mkdir(exist_ok=True)

    index_A = 0
    # Iter rows
    for id_A in pdbe_chain_ids:

        # Load UniProt residue indices for structure in column
        unps_A = io_utils.serial_load(unp_res_ids[id_A])

        # CA distance matrix in column position
        score_row = []  # Scores along matrix row
        label_row = []  # Structure comparison labels along matrix row

        index_B = 0
        # Fill the row
        for id_B in pdbe_chain_ids:

            if index_A < index_B:

                # Load UniProt residue indices for structure in column
                unps_B = io_utils.serial_load(unp_res_ids[id_B])

                # CALCULATE SUM OF DISTANCE DIFFERENCE MATRIX UPPER TRIANGLE
                # UNP indices are matched but ends of largest must be trimmed
                path_save_file = None
                if path_save:
                    fname = f"{unp}_{id_A}_to_{id_B}"
                    path_save_file = path_save.joinpath(fname)

                # Note: "dd_matx" = "distance difference matrix"
                dd_matx = distance_differences.generate_matx_diff(
                    io_utils.load_matrix(ca_matxs[id_A]),
                    io_utils.load_matrix(ca_matxs[id_B]),
                    path_save=path_save_file,
                    return_matx=True,
                )

                # Calculate score
                score = linear_algebra_utils.calc_score(dd_matx, unps_A, unps_B)
                score_row.append(score)

                # del dd_matx
                # progress.update()

            else:
                score_row.append(0)

            # Store label for score
            score_label = f"{id_A}_to_{id_B}"
            label_row.append(score_label)

            index_B += 1

        # Store matrices to list
        score_matx.append(score_row)
        label_matx.append(label_row)

        index_A += 1

    # Converting lists to Numpy arrays for performance in other methods
    score_matx = asarray(score_matx)

    return maximum(score_matx, score_matx.transpose()), asarray(label_matx)


def make_linkage_matx(model: AgglomerativeClustering) -> "ndarray[any, float]":
    """
    Linkage matrix returned from a SKLearn model for agglomerative clustering.
    Returned: Numpy ndarray

    :param model: SKLearn agglomerative clustering model, fitted to some score matrix.
    :type model: sklearn.cluster.AgglomerativeClustering
    :return: Matrix of descriptors for the clustering results from the SKLearn model.
    :rtype: np.ndarray[any, float]
    """

    counts = zeros(model.children_.shape[0])
    n_samples = len(model.labels_)
    for i, merge in enumerate(model.children_):
        current_count = 0
        for child_idx in merge:
            if child_idx < n_samples:
                current_count += 1  # leaf node
            else:
                current_count += counts[child_idx - n_samples]
        counts[i] = current_count

    linkage_matrix = column_stack([model.children_, model.distances_, counts]).astype(
        float
    )

    return linkage_matrix


def cluster_agglomerative(
    score_matx: "ndarray[any, float]", cutoff: float = None
) -> AgglomerativeClustering:
    """
    Performs the agglormerative (bottom up) clustering algorithm on a given score
    matrix.

    :param score_matx: N*N-square, symmetric matrix of scores used for clustering.
    :type score_matx: ndarray[any, float]
    :param cutoff: Threshold below which to begin defining groups as clusters, defaults
        to None
    :type cutoff: float, optional
    :return: Scikit-learn Agglomerative model fitted to input data.
    :rtype: sklearn.cluster.AgglomerativeClustering
    """

    # Get the cutoff
    model = AgglomerativeClustering(
        n_clusters=None,  # Force function to fix number of clusters
        distance_threshold=0,
        affinity="precomputed",  # Force to use score matrix
        linkage="average",  # UPGMA
        compute_distances=True,
    )

    # Repeat but cluster based on parsed cutoff
    if cutoff:

        # Fit score matrix to model
        model = model.fit(score_matx)
        linkage_matx = make_linkage_matx(model)

        cutoff *= linkage_matx[:, 2].max()

        model = AgglomerativeClustering(
            n_clusters=None,
            distance_threshold=cutoff,
            affinity="precomputed",
            linkage="average",
            compute_distances=True,
        )

    # Fit score matrix to model
    model = model.fit(score_matx)

    return model


def format_dendrogram(ax: Axes, unp: str) -> None:
    """
    Apply aesthetic features to the dendrogram.

    :param ax: Figure axis to plot data
    :type ax: Axes
    :param unp: UniProt accession
    :type unp: str
    """

    ax.set_title(f"Agglomerative clustering dendrogram: {unp}", fontweight="bold")
    ax.set_ylabel("Score (\u212B)")


def plot_dendrogram(
    unp: str, linkage_matrix: ndarray = None, cutoff: float = None, **kwargs
) -> "tuple(Figure, Axes)":
    """
    Create linkage matrix from SKLearn model and plot the dendrogram of nodes.

    :param unp: UniProt accession
    :type unp: str
    :param linkage_matrix: Matrix of clustering information (labels and distances)
        derived from the fitted agglomerative clustering model.
    :type linkage_matrix: np.ndarray
    :param cutoff: Distance threshold below which clusters were defined, has no affect
        on cluster results and is simply used to plot horizontal line for clarity.
        Should be 0-1, defaults to None.
    :param kwargs: Same parameters as accepted by scipy.cluster.hierarchy.dendrogram()
    :type kwargs: Any
    :type cutoff: float, optional
    :return: Matplotlib figure and axis.
    :rtype: tuple(matplotlib.figure.Figure, matplotlib.axes.Axes)
    """

    _, axis = plt.subplots(1, 1)

    # Plot the corresponding dendrogram
    dendrogram(linkage_matrix, ax=axis, **kwargs)

    # Add horizontal line where cutoff is placed
    if cutoff:
        max_parent = max(linkage_matrix[:, 2])
        axis_xlimits = axis.get_xlim()
        axis.hlines(
            y=max_parent * cutoff,
            xmin=axis_xlimits[0],
            xmax=axis_xlimits[1],
            colors=["black"],
            linestyles=["dashed"],
            linewidths=1,
            alpha=0.5,
        )

    format_dendrogram(axis, unp)


def format_swarmplot(ax: Axes, unp: str) -> None:
    """
    Add custom formatting features to swarm plot.

    :param ax: _description_
    :type ax: Axes
    :param unp: _description_
    :type unp: str
    """
    ax.set_title(unp, fontweight="bold")
    ax.set_ylabel("Score (\u212B)")


def plot_swarmplot(y_data: Iterable, unp: str) -> "tuple(Figure, Axes)":
    """Creates a strip plot of non-overlapping data points for a given list of data. The
    values of the data will correspond to their y-values. Their position along the
    x-axis is irrelevant as they're all identical.

    :param y_data: Array of data points to plot.
    :type y_data: Iterable
    :param unp: UniProt accession.
    :type unp: str
    :return: Figure and axis objects containing the plotted swarm plot
    :rtype: tuple(matplotlib.figure.Figure, matplotlib.axes.Axes)
    """
    # Init the figure
    _, ax = plt.subplots(
        1,
        1,
        figsize=(4, 5),
        # ncols=1,    # INTRA | bar | INTER | bar
        # nrows=1,
        # gridspec_kw=dict(width_ratios=[4, 0.2, 4, 0.2]),
        tight_layout=True,
    )
    # Plot the data
    sns.swarmplot(data=y_data, ax=ax, size=5)  # vmax=max_dist ,

    # Add some formatting
    format_swarmplot(ax, unp)
