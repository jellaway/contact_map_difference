"""
The class here performs clustering on a set of parsed chains.
"""

# Third party imports
from logging import getLogger
from pathlib import Path

import numpy as np
from matplotlib import use as mpl_use
from matplotlib.pyplot import subplots
from pandas import DataFrame

# Custom imports -- peptide_analysis
from cluster_conformers import cluster_chains, distance_differences

# Custom imports -- utils
from cluster_conformers.utils import (
    appearance_utils,
    download_utils,
    io_utils,
    linear_algebra_utils,
    parsing_utils,
)

# Global variable
CLUSTERING_CUTOFF_PC = 0.7
logger = getLogger(__name__)
mpl_use("AGG")


class ClusterConformations:
    """
    Object to collect clustering information from a parsed list of paths to updated
    mmCIF files.

    The object contains methods to create CA distance matrices, distance difference
    matrices, a CSV file containing cluster assignments for parsed mmCIFs, dendrogram
    of clustered chains, plots (as 2D histograms) of distance difference maps, and swarm
    plots of scores for chain-chain comparisons.

    The object relies on functions used throughout the codebase to perform conformation
    clustering on any given list of mmCIF files.

    TODO:
     - Implement cluster variable measure per residue
     - Add function to plot cluster variability and between-cluster variability features
    """

    # Constructor
    def __init__(
        self,
        unp: str,
        mmcifs_and_chains: "dict[str, list[str] ]",
        path_save_alphafold: Path = None,
    ) -> None:
        """
        Constructor -- setup object.
        Loads mmCIFs, PDBe IDs, chains IDs, and paths parsed into script.

        :param unp: UniProt accession
        :type unp: str
        :param mmcifs_and_chains: Dictionary of paths to updated mmCIF files (keys) and
            the desired chain (values) as label_asym_ids in a list.
        :type mmcifs_and_chains: dict[str, list[str] ]
        :param path_save_alphafold: Path to save downloaded AlphaFold structre, acting
            as a flag to include it in clustering or not, defaults to None
        :type path_save_alphafold: Path, optional
        """

        self.unp = unp  # UniProt accession
        # self.mmcifs = {         # Loaded mmCIF objects
        #     # "1atp" : gemmi_block_obj (loaded mmcif),
        #     # "2adp" : gemmi_block_obj (loaded mmcif),
        #     # ...
        # }
        self.mmcif_paths = {  # Paths to mmCIFs
            # "1atp" : path to mmCIF as str,
            # "2adp" : path to mmCIF as str,
            # ...
        }
        self.chains = {  # Non-redundant list of desired chains
            # "1atp" : ["A", "B", ...],
            # "2adp" : ["A", "B", ...],
            # ...
        }
        self.chains_all = []  # Redundant list of chain IDs+
        self.pdbe_ids = []  # Non-redundant list of PDBe names. Ordered
        # once per parsed chain). Ordered

        # Track progress
        logger.info(f"Loading mmCIF files for {self.unp}")

        # progress = appearance_utils.ProgressBar(
        #     len(mmcifs_and_chains)
        # )

        # Load mmCIF files into object
        for mmcif_path, chains in mmcifs_and_chains.items():

            # Only load mmCIFs
            if (mmcif_path[-3:] == "cif") or (mmcif_path[-6:] == "cif.gz"):

                # Store PDBe ID
                mmcif_fname = mmcif_path.split("/")[-1]
                pdbe_id = mmcif_fname[:4]
                self.pdbe_ids.append(pdbe_id)

                # Store list of chains
                self.chains[pdbe_id] = chains

                self.mmcif_paths[pdbe_id] = mmcif_path

                # Record (redundant) order of files
                self.chains_all += chains
                # progress.update()

        # Handle pre-clustering AlphaFold file information
        if path_save_alphafold:

            # Download and save
            afdb_path = download_utils.download_alphafold_mmcif(
                self.unp, path_save_alphafold
            )

            # Only add AlphaFold structure info if not 404 error
            if afdb_path:

                # Rename for clustering compatibility with archive naming scheme
                af_prefix = "afv3"
                af_fname = f"{af_prefix}_updated.cif"
                io_utils.rename_file(afdb_path, af_fname)

                # Load AlphaFold structure
                path_alphafold = afdb_path.parent.joinpath(af_fname)
                afdb_structure = io_utils.load_mmcif(path_alphafold)

                # Store object for clustering in later methods
                # self.mmcifs[af_prefix] = afdb_structure
                self.mmcif_paths[af_prefix] = str(path_alphafold)
                self.pdbe_ids.append(af_prefix)

                # Record chain info for clustering in later methods
                af_chain = "A"
                self.chains[af_prefix] = [af_chain]
                self.chains_all.append(af_chain)

                # Parse AlphaFold structure for extracting chain info for superpose.py
                afdb_mmcif = parsing_utils.parse_mmcif(afdb_structure, af_chain)
                # Storing the start-end UniProt residue indices for protein-superpose
                self.af_unp_range = (
                    afdb_mmcif["unp_res_ids"][0],
                    afdb_mmcif["unp_res_ids"][-1],
                )

        # Conversion to Numpy array for performance in other functions
        self.chains_all = np.asarray(self.chains_all)

        # Set matplotlib global formatting
        appearance_utils.init_plot_appearance()

    def ca_distance(self, path_save: Path = None) -> None:
        """
        Calculates a pairwise CA distance matrix for all parsed mmCIF/chains. Stores
        matrices in dictionary and saves matrix files if path provided.

        :param path_save: Path to save CA distance matrices, defaults to None
        :type path_save: pathlib.Path, optional
        """

        self.ca_matxs = {  # CA distance matrices. Ordered
            # "1atp_A" : path (as pathlib.Path) to serilised np.ndarray(...) file,
            # "2adp_B" : path (as pathlib.Path) to serilised np.ndarray(...) file ...
            # ...
        }

        self.unp_res_ids = {
            # "1atp_A" : path (as pathlib.Path) to serilised np.array(),
            # "2adp_B" : path (as pathlib.Path) to serilised np.array() ...
            # ...
        }
        # Dir to save the raw UniProt residue IDs as 1D np.array()s
        path_save_unps = path_save.joinpath("unp_residue_ids")
        path_save_unps.mkdir(exist_ok=True)

        self.pdbe_chain_ids = []  # String of pdbID_chainID. Ordered

        # Track progress
        logger.info("Calculating CA distance matrices...")
        # progress = appearance_utils.ProgressBar(
        #     len(self.chains_all)
        # )

        # Generate CA matrices from loaded mmCIF files (only for parsed chain IDs)
        for pdbe_id in self.pdbe_ids:

            for chain in self.chains[pdbe_id]:

                # Make unique PDBe-chain ID
                pdbe_chain_id = f"{pdbe_id}_{chain}"

                # Extract x, y, z, and UNP index info from mmCIF/chain
                mmcif = io_utils.load_mmcif(self.mmcif_paths[pdbe_id])
                xyz_unp_dict = parsing_utils.parse_mmcif(mmcif, chain)

                # Store PDB_chainID
                self.pdbe_chain_ids.append(pdbe_chain_id)

                # Serialise the unimputed array of UniProt residue indices
                path_save_this_unp = path_save_unps.joinpath(f"{pdbe_chain_id}.pickle")
                io_utils.serial_dump(xyz_unp_dict["unp_res_ids"], path_save_this_unp)

                # Store reference to serialised UNP residue ID  object
                self.unp_res_ids[pdbe_chain_id] = path_save_this_unp

                # Make square CA matrix
                xyz_unp_dict = parsing_utils.fill_missing_unps(xyz_unp_dict)
                ca_matx = linear_algebra_utils.generate_ca_matx(
                    xyz_unp_dict["cartn_x"],
                    xyz_unp_dict["cartn_y"],
                    xyz_unp_dict["cartn_z"],
                )

                # Write matrix file if specified
                if path_save:
                    out_file_name = f"{pdbe_chain_id}_ca_distance_matrix"
                    out_path = path_save.joinpath(out_file_name)
                    io_utils.save_matrix(ca_matx, out_path)
                    # Reference path to saved matrix -- loaded in later
                    self.ca_matxs[pdbe_chain_id] = Path(str(out_path) + ".npz")

                del ca_matx
                # progress.update()

        # Convert to Numpy array for performance improvement
        self.pdbe_chain_ids = np.asarray(self.pdbe_chain_ids)

    def cluster(
        self, path_save_dd_matx: Path = None, path_save_cluster_results: Path = None
    ) -> None:
        """
        Clusters the chains parsed by sum-based score. Clustering results are stored to
        the object instance under the following accessible attributes:
         - self.cluster_df : pandas.DataFrame of per-chain clustering results in tabular
            format
         - self.cluster_dict : dictionary of per-chain clustering results, predominently
            used by class in `protein-superpose`

        Also writes distance difference matrices and clustering results if paths parsed
        as arguments.

        :param path_save_dd_matx: Path to save calculated CA distance-difference
            matrices, defaults to None
        :type path_save_dd_matx: pathlib.Path, optional
        :param path_save_cluster_results: pathlib.Path to save clustering results, defaults to
            None
        :type path_save_cluster_results: pathlib.Path, optional
        """

        # ca_fnames = get_fnames(path_ca_matxs)
        logger.info("Generating distance difference matrices...")
        self.score_matx, self.label_matx = cluster_chains.build_inputs(
            self.unp,
            self.pdbe_chain_ids,
            self.unp_res_ids,
            path_save_dd_matx,
            self.ca_matxs,
        )
        # If all scores==0, place all chains into a single cluster
        if not np.all(self.score_matx == 0):
            logger.info("Non-zero score matrix")

            # Begin clustering
            logger.info("Clustering structures into conformational states...")
            self.model = cluster_chains.cluster_agglomerative(
                self.score_matx, cutoff=CLUSTERING_CUTOFF_PC
            )

            self.linkage_matx = cluster_chains.make_linkage_matx(self.model)

            # Cluster labels
            cluster_labels = self.model.labels_

        else:
            logger.info("All zero score matrix")

            # Cluster labels
            cluster_labels = np.full(self.chains_all.shape[0], 0)
            self.linkage_matx = np.array([0])

        # Store clustering results to object as table
        unps_redundant = np.full(self.chains_all.shape[0], self.unp)
        pdbes = self.pdbe_chain_ids.astype("S4")

        # Output clustering results table
        self.cluster_df = DataFrame(
            {
                "UNP_ACC": unps_redundant,
                "PDBe_ID": pdbes.astype("<U6"),
                "CHAIN_ID": self.chains_all,
                "CONFORMER_ID": cluster_labels,
            }
        )

        # Add clustering results to dictionary for parsing into process_clusters in
        # protein-superpose
        self.cluster_dict = {}
        for conformer_id in np.flip(self.cluster_df["CONFORMER_ID"].unique()):
            # Conformer ID order flipped to preserve compatibility with
            tmp_df = self.cluster_df[self.cluster_df["CONFORMER_ID"] == conformer_id]

            self.cluster_dict[conformer_id] = list(
                tmp_df["PDBe_ID"] + tmp_df["CHAIN_ID"]
            )

        # Write out clustering results if path specified
        if path_save_cluster_results:

            # Save clustering results
            path_save_all_conf = path_save_cluster_results.joinpath(
                f"{self.unp}_sum_based_clustering_results.csv"
            )
            self.cluster_df.to_csv(path_save_all_conf, index=False)

            # File names for score, label and linkage matrices
            labels = [
                path_save_cluster_results.joinpath(f"{self.unp}_score_matrix"),  # Score
                path_save_cluster_results.joinpath(f"{self.unp}_label_matrix"),  # Label
                path_save_cluster_results.joinpath(
                    f"{self.unp}_linkage_matrix"  # Linkage
                ),
            ]

            # Matrices from clustering results
            cluster_matrices = [
                self.score_matx,  # Score
                self.label_matx,  # Label
                self.linkage_matx,  # Linkage
            ]

            # Save clustering matrices
            for label, matx in zip(labels, cluster_matrices):
                io_utils.save_matrix(matx, label)

        logger.info("Clustering done.")

    def select_representatives(self) -> None:
        """
        Method to select representative structures from the set of predicted clusters.

        TODO:
         - Find runtime improvements, currently very slow
         - Must be either saved in DataFrame format or merged with the exhisting
           clustering CSV
         - Make toggleable.
        """

        self.representatives = {}

        for conformer_id in self.cluster_df["CONFORMER_ID"].unique():
            # Show info to the user
            logger.info(
                "Identifying representative structures for predicted conformer "
                f"{conformer_id}"
            )

            # Select rows in predicted conformation
            chains_in_conformer = self.cluster_df[
                self.cluster_df["CONFORMER_ID"] == conformer_id
            ]

            # Convert to iterable of PDB ID - chain. E.g. 1pdb_A
            pdbe_chain_ids = (
                chains_in_conformer["PDBe_ID"] + "_" + chains_in_conformer["CHAIN_ID"]
            )

            # Create list of CA matrices
            ca_matxs_in_cluster = [self.ca_matxs.get(key) for key in pdbe_chain_ids]

            # Select the smallest max index position to make truncation
            max_index_list = []
            for ca_matx in ca_matxs_in_cluster:
                max_index_list.append(io_utils.serial_load(ca_matx).shape[0])
            truncation_index = min(max_index_list)  # Set truncation value

            # Find all median values
            # Relatively slow -- a performance improvement might be needed here
            logger.info("Generating matrix of element-wise medians")
            # progress = appearance_utils.ProgressBar(truncation_index**2)
            col_index = 0
            median_matrix = []
            while col_index < truncation_index:
                row_index = 0
                median_matrix_row = []

                while row_index < truncation_index:
                    median_at_index = np.median(
                        [
                            io_utils.serial_load(x)[col_index][row_index]
                            for x in ca_matxs_in_cluster
                        ]
                    )
                    median_matrix_row.append(median_at_index)
                    # progress.update()
                    row_index += 1

                median_matrix.append(median_matrix_row)
                col_index += 1

            median_matrix = np.asarray(median_matrix_row)

            # Find number of medians per CA matrix
            median_counts_per_chain = {}
            max_num_medians = 0
            for id in pdbe_chain_ids:

                ca_matx = io_utils.serial_load(self.ca_matxs[id])
                ca_matx_truncated = linear_algebra_utils.matx_trim(
                    ca_matx, truncation_index
                )

                # Find number of medians and store
                num_medians = np.count_nonzero(ca_matx_truncated == median_matrix)
                median_counts_per_chain[id] = num_medians

                # Keep track of the highest number of medians in a given CA matrix
                if num_medians > max_num_medians:
                    max_num_medians = num_medians

            representative_chains = {}
            median_set = False
            for id, median in median_counts_per_chain.items():
                if (median == max_num_medians) and (not median_set):
                    representative_chains[id] = True
                    median_set = True
                else:
                    representative_chains[id] = False

            # Assign representative chains dict to field
            self.representatives.update(representative_chains)

            # Debug
            for key, value in representative_chains.items():
                if value:
                    logger.info(f"Representative for conformer {conformer_id} = {key}")

    def make_dendrogram(
        self, path_save: Path, png: bool = False, svg: bool = False
    ) -> None:
        """
        Plot hierachical dendrogram from clustering results. Must have created model
        object using cluster() method first.

        :param path_save: Path to save rendered dendrogram image.
        :type path_save: pathlib.Path
        :param png: Save dendrogram image in PNG format, defaults to False
        :type png: bool, optional
        :param svg: Save dendrogram image in SVG format, defaults to False
        :type svg: bool, optional
        """

        if not np.array_equal(self.linkage_matx, np.array([0])):

            logger.info("Rendering dendogram")
            cluster_chains.plot_dendrogram(
                self.unp,
                self.linkage_matx,
                CLUSTERING_CUTOFF_PC,
                labels=self.pdbe_chain_ids,
                leaf_rotation=90,
            )  # p=3

            io_utils.save_figure(
                path_save,
                save_fname=f"{self.unp}_agglomerative_dendrogram",
                png=png,
                svg=svg,
            )

        else:
            logger.info("Single cluster for segment. Not rendering dendrogram.")

    def make_swarmplot(
        self, path_save: Path, png: bool = False, svg: bool = False
    ) -> None:
        """
        Plot scores as a swarm plot. Must have created model object using cluster()
        method first.

        TODO:
         - Add colours to data points based on clustering results

        :param path_save: Path to save rendered swarm plot
        :type path_save: pathlib.Path
        :param png: Save swarm plot image in PNG format, defaults to False
        :type png: bool, optional
        :param svg: Save swarm plot image in SVG format, defaults to False
        :type svg: bool, optional
        """

        # Flatten score matrix to 1D vector of unique elements only
        scores = linear_algebra_utils.upper_triangle(self.score_matx, 1, numeric=True)

        swarm_plot = cluster_chains.plot_swarmplot(scores, self.unp)

        io_utils.save_figure(
            path_save, save_fname=f"{self.unp}_swarm_plot", png=png, svg=svg
        )

        # Flush from memory
        del swarm_plot

    def make_dd_maps(self, path_save: Path) -> None:
        """
        Plot all unique distance difference matrices as 2D histograms (heatmaps). Must
        have created model object using cluster() method first.

        :param path_save: Path to save CA distance-diiference heatmaps.
        :type path_save: pathlib.Path
        """

        # Flatten matrices into 1D vectors of unique elements
        dd_matxs = linear_algebra_utils.upper_triangle(self.dd_matxs, 1, numeric=False)
        dd_labels = linear_algebra_utils.upper_triangle(
            self.label_matx, 1, numeric=False
        )
        dd_scores = linear_algebra_utils.upper_triangle(
            self.score_matx, 1, numeric=True
        )

        heatmap_kwargs = distance_differences.make_heatmap_kwargs()

        # Find the maximum distance in the UniProt group
        max_distance = 0
        for dd_matx in dd_matxs:
            max_distance = linear_algebra_utils.find_max(
                io_utils.serial_load(dd_matx), starting_max=max_distance
            )

        heatmap_kwargs["vmax"] = max_distance

        # Track progress
        logger.info("Rendering distance difference maps")
        # progress = appearance_utils.ProgressBar( len(dd_matxs) )

        # Plot distance difference maps
        for dd_matx, title, score in zip(dd_matxs, dd_labels, dd_scores):

            fig, axes = subplots(
                # figsize=(10,3),
                ncols=2,  # matrix | colour_bar
                nrows=1,
                gridspec_kw={"width_ratios": [4, 0.2]},
                tight_layout=True,
            )

            distance_differences.plot_2d_hist(
                io_utils.serial_load(dd_matx), axes, heatmap_kwargs
            )

            distance_differences.add_colour_bar(fig, axes)

            figure_title = distance_differences.format_title(
                self.unp, self.cluster_df, title[:4], title[10:14], title[5], title[15]
            )

            distance_differences.format_2d_hist(
                axes, figure_title, score  # Data  # Labels  # ... more labels
            )

            io_utils.save_figure(
                path_save,
                save_fname=f"{self.unp}_{title}",
                png=True,
                svg=False,
            )

            # progress.update()

        logger.info("Rendering done.")
