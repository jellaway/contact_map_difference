import numpy as np


def rolling_window(matx, window_size=8,mu=2.3335763555048907, two_sigma=2*3.7479942824329044):

    background_suppressed = []

    col_num = len(matx[0])
    last_window_size = col_num % window_size
    last_window_start_index = col_num - last_window_size

    inverse_covariance = mu/two_sigma

    
    for row in matx:

        background_suppressed_row = []

        # Make a slice from the diagonal to the end of the matrix, therefore only
        # focussing on the upper triangle for the rolling window
        
        # Define initial window index positions
        start_index = 0
        end_index = start_index + window_size

        # First window
        while end_index <= last_window_start_index:
            
            row_window = row[start_index : end_index]

            row_window = row_window[~np.isnan(row_window)]
            # print(row_window)

            window_mean = np.mean(row_window)
            window_stdev = np.mean(row_window)

            if (window_mean < mu) and (window_stdev < two_sigma): 
                background_suppressed_row += [0]*window_size
            else:
                background_suppressed_row += [window_mean]*window_size

            start_index += window_size
            end_index += window_size
        
        background_suppressed.append(background_suppressed_row)
        start_index += 1

        row_window = row[last_window_start_index:]

        row_window = row_window[~np.isnan(row_window)]
        # print(row_window)

        window_mean = np.mean(row_window)
        window_stdev = np.mean(row_window)

        if (window_mean < mu) and (window_stdev < two_sigma):
            background_suppressed_row += [0]*last_window_size
        else:
            background_suppressed_row += [window_mean]*last_window_size

    return np.asarray(background_suppressed)


# def collate_master_vector(matx_list, res_mask=-1, path=pathlib.Path.home(), save=False):
#     """
#     Parse a list of file names (including their path, preferably) for a set of .npy 
#     files containing N*N-dimensional matrices. A 1-dimensional master array is  created
#     containing all elements from all matrices pertaining to the list of files names. 
#     NaN values in the matrix are excluded from the master array. 

#     These will be very large files but are needed for plotting 1D histograms downstream.

#     Return type: 1D Numpy array 
#     """
#     # Initialise return variable
#     master_list = []
#     maximum = len(matx_list)
#     progress = appearance_utils.ProgressBar(maximum)

#     # Iterate matrices in list parsed list of matrices
#     for matx in matx_list:
#         progress.update()

#         # Mask n neighbouring residues
#         if res_mask >= 0:

#             # Apply residue mask
#             master_list += linear_algebra.linearise(matx, res_mask)

#         # No mask
#         else:
#             # Loop rows
#             for i in matx:
#                 # Loop columns
#                 for j in i:
#                     # Ignore NaN values
#                     if not np.isnan(j):
#                         master_list.append(j)
                        
#     if save:
#         print("Saving master array. This could take several moments...")
#         save_matrix(np.asarray(master_list), path)
#         print("Master array saved as:", path, '\n')
#     else:
#         return np.asarray(master_list)