

# from cgi import test
# from statistics import stdev
import numpy as np
from matplotlib import pyplot as plt




if __name__ == "__main__":


    test_matx = np.load(
        # "/home/jellaway/EMBL-EBI/funclan_work/static-conformer-dataset/intRA_distance_difference/P40131_3vjp_A_to_3vki_B_difference.npy"
        "/home/jellaway/EMBL-EBI/funclan_work/static-conformer-dataset/intER_distance_difference/P40131_3tee_A_to_3vki_A_difference.npy"
        )

    # print(test_matx)
    # test_matx = test_matx[~np.isnan(test_matx)]

    cv_list = []

    counter = 0
    max_index = test_matx.shape[0]
    # print(max_index)

    while counter < max_index:

        row = test_matx[counter][counter:]

        column = test_matx[:,counter][:counter]

        distances = np.concatenate( (row, column) )
        distances = distances[~np.isnan(distances)]

        mean = np.mean(distances)**2
        stddev = np.std(distances)
        print(stddev)

        cv = mean/stddev

        cv_list.append(np.median(distances))

        counter += 1
    
    plt.plot(cv_list)


    test_matx = np.load(
        "/home/jellaway/EMBL-EBI/funclan_work/static-conformer-dataset/intRA_distance_difference/P40131_3vjp_A_to_3vki_B_difference.npy"
        # "/home/jellaway/EMBL-EBI/funclan_work/static-conformer-dataset/intER_distance_difference/P40131_3tee_A_to_3vki_A_difference.npy"
        )

    cv_list = []

    counter = 0
    max_index = test_matx.shape[0]
    # print(max_index)

    while counter < max_index:

        row = test_matx[counter][counter:]

        column = test_matx[:,counter][:counter]

        distances = np.concatenate( (row, column) )
        distances = distances[~np.isnan(distances)]

        mean = np.mean(distances)**2
        stddev = np.std(distances)
        print(stddev)

        cv = mean/stddev

        cv_list.append(np.median(distances))

        counter += 1

    plt.plot(cv_list)

    plt.show()

        


    
    # print(test_matx[:,20])