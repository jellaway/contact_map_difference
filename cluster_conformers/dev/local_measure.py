

import numpy as np
import pathlib
from ..tools.parsing_tools import *

from matplotlib import pyplot as plt
import scipy


def exp_func(x, b, c):
    return np.exp(b * x) + c


if __name__ == "__main__":

    path_dd_matxs = pathlib.Path(
        # "benchmark_data/examples/O34926/O34926_distance_differences"
        # "benchmark_data/all_uniprots/distance_differences/within_conformations"
        "benchmark_data/all_uniprots/distance_differences/between_conformations"

    )

    dd_fnames = get_fnames(path_dd_matxs)

    dd_matxs = []

    for dd_fname in dd_fnames:
        
        path_dd_matx = path_dd_matxs.joinpath(dd_fname)

        dd_matx = load_matrix(path_dd_matx)

        upper_triangle = linearise(dd_matx, 1)

        upper_triangle_sorted = np.sort(upper_triangle)
        print(upper_triangle_sorted)

        fit, fit_data = scipy.optimize.curve_fit(
            exp_func, 
            range(1, upper_triangle_sorted.shape[0]+1), 
            upper_triangle_sorted
            # bounds=(0, upper_triangle_sorted[-1])
            )
        print("####################")
        b, c = fit
        print(b, c)
        

        # plt.plot(np.log10(upper_triangle_sorted))
        plt.plot(range(1, upper_triangle.shape[0]+1), upper_triangle_sorted)
        # plt.plot(range(1, upper_triangle.shape[0]+1), exp_func(range(1, upper_triangle.shape[0]+1), *fit))

        plt.title(dd_fname)

        plt.show()

        plt.close()






    