
import numpy as np

def stirling2k(n,k):
    # Stirling Algorithm
    # Cod3d by EXTR3ME
    # https://extr3metech.wordpress.com
    n1=n
    k1=k
    if n<=0:
        return 1
     
    elif k<=0:
        return 0
     
    elif (n==0 and k==0):
        return -1
     
    elif n!=0 and n==k:
        return 1
     
    elif n<k:
        return 0
    # Preventing stack overflow
    elif k==2 and n>0:
        return (2**(n-1) - 1)
    
    elif k==3 and n>0:
        try:
            power = n-1
            return 3**power - 2**power - (3**power - 1)/2
        except OverflowError:
            return np.nan

    elif k==4 and n>0:
        try:
            power = n-1
            return (4**power - 1)/6 - (3**power - 2**power)/2
        except OverflowError:
            return np.nan
 
    else:
        temp1=stirling2k(n1-1,k1)
        temp1=k1*temp1
        return (k1*(stirling2k(n1-1,k1)))+stirling2k(n1-1,k1-1)