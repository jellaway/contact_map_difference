#!/bin/sh

python3.7 find_conformers.py -u "O34926" \
    -m benchmark_data/examples/O34926/O34926_updated_mmcif/3nc3_updated.cif A B \
    -m benchmark_data/examples/O34926/O34926_updated_mmcif/3nc5_updated.cif A B \
    -m benchmark_data/examples/O34926/O34926_updated_mmcif/3nc6_updated.cif A B \
    -m benchmark_data/examples/O34926/O34926_updated_mmcif/3nc7_updated.cif A B \
    -c benchmark_data/examples/O34926/O34926_ca_distances/ \
    -d benchmark_data/examples/O34926/O34926_distance_differences/ \
    -s benchmark_data/examples/O34926/O34926_cluster_results/ \
    -g benchmark_data/examples/O34926/O34926_cluster_results/ png svg \
    -a benchmark_data/examples/O34926/O34926_alpha_fold_mmcifs

# -o benchmark_data/examples/O34926/O34926_distance_difference_maps/ \
# -w benchmark_data/examples/O34926/O34926_cluster_results/ png svg \
