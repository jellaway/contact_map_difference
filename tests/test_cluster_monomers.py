"""
Unit tests for the main object which handles clustering of monomeric protein chains
"""

# Third party imports
# import os
import pathlib

# import sys
import unittest
from unittest import mock

import gemmi
import numpy as np
import pandas as pd

# Import functions/classes to test
from cluster_conformers import cluster_monomers

# Import modified TestCase class
from .test_case import TestCaseModified, remove_files

# # Modify Python path for custom imports
# sys.path[0] = str(  # Override default Python import part
#     pathlib.Path(
#         os.path.realpath(__file__)  # Path to this file
#     ).parent.parent  # Path to folder this file is in
# )


# Global variables

# Initialise base path as string
PATH_HOME = pathlib.Path.home()
PATH_BASE = PATH_HOME.joinpath(
    "EMBL-EBI", "funclan_work", "contact_map_difference", "tests"
)
PATH_SERIALISED_MATXS = PATH_BASE.joinpath("mock_serial_matxs")
PATH_SERIALISED_10_10_MATXS = PATH_SERIALISED_MATXS.joinpath("10_by_10_full")

# Initialise other paths
PATH_SAVE_CA = PATH_BASE.joinpath("test_output/ca_distances")
PATH_SAVE_CLUSTER = PATH_BASE.joinpath("test_output/cluster_results")
PATH_DD_MATXS = PATH_BASE.joinpath("test/distance_differences")
PATH_SAVE_DD_MAPS = PATH_BASE.joinpath("test_output/distance_difference_maps")

PATH_TRUNCATED_MOCK_MMCIFS = f"{str(PATH_BASE)}/mock_data/mock_mmcifs/"


# Mock objects
LOADED_MMCIF_OBJ_TYPE = gemmi.cif.Block
CHAINS = ["A", "B"]
TEST_UNP = "A12345"
EXPECTED_OBJ = mock.Mock(
    name="Mock ClusterConformers() object",
    **{
        "unp": TEST_UNP,
        "mmcifs": {
            "3nc3": LOADED_MMCIF_OBJ_TYPE,
            "3nc5": LOADED_MMCIF_OBJ_TYPE,
            "3nc6": LOADED_MMCIF_OBJ_TYPE,
            "3nc7": LOADED_MMCIF_OBJ_TYPE,
        },
        "mmcif_paths": {
            "3nc3": f"{PATH_TRUNCATED_MOCK_MMCIFS}3nc3_updated.cif",
            "3nc5": f"{PATH_TRUNCATED_MOCK_MMCIFS}3nc5_updated.cif",
            "3nc6": f"{PATH_TRUNCATED_MOCK_MMCIFS}3nc6_updated.cif",
            "3nc7": f"{PATH_TRUNCATED_MOCK_MMCIFS}3nc7_updated.cif",
        },
        "chains": {"3nc3": CHAINS, "3nc5": CHAINS, "3nc6": CHAINS, "3nc7": CHAINS},
        "chains_all": np.array(["A", "B", "A", "B", "A", "B", "A", "B"]),
        "pdbe_ids": ["3nc3", "3nc5", "3nc6", "3nc7"],
    },
)

# Test input
TEST_MMCIFS_AND_CHAINS_DICT = {
    f"{PATH_TRUNCATED_MOCK_MMCIFS}3nc3_updated.cif": CHAINS,
    f"{PATH_TRUNCATED_MOCK_MMCIFS}3nc5_updated.cif": CHAINS,
    f"{PATH_TRUNCATED_MOCK_MMCIFS}3nc6_updated.cif": CHAINS,
    f"{PATH_TRUNCATED_MOCK_MMCIFS}3nc7_updated.cif": CHAINS,
}


class TestClusterMonomers(TestCaseModified):
    """
    Tests for the script that handles calculations for finding distance differences
    """

    def setUp(self):
        """
        Creates a simplistic class instance of a set of truncated protein chains. The
        instance is used by all functions, which are subsequently patched where
        function calls are tested elsewhere.
        """

        # Initialise common object instance
        self.test_obj = cluster_monomers.ClusterConformations(
            unp=TEST_UNP, mmcifs_and_chains=TEST_MMCIFS_AND_CHAINS_DICT
        )

        # Patch out the call of download_alphafold_mmcif()
        # self.test_obj_af = cluster_monomers.ClusterConformations(
        #     unp=TEST_UNP,
        #     mmcifs_and_chains=TEST_MMCIFS_AND_CHAINS_DICT,
        #     path_save_alphafold=PATH_TRUNCATED_MOCK_MMCIFS

        # )
        return super().setUp()

    def tearDown(self):
        """
        Flush class instance after each test.
        """
        return super().tearDown()

    ####################################################################################
    # Decorators needed
    ####################################################################################
    def test_init(self):
        """
        Checks if the class instance is generated correctly upon initialisation.
        """

        # # Check mmCIFs were saved as Gemmi Block objects in dict
        # for key, val in EXPECTED_OBJ.mmcifs.items():

        #     # Check all keys have been stored
        #     self.assertTrue(
        #         key in self.test_obj.mmcifs.keys(),
        #         msg="PDB ID has not been stored as key in the "
        #         "ClusterConformations.mmcifs dict.",
        #     )

        #     # Check values are stored as Gemmi Block objects
        #     self.assertIsInstance(
        #         self.test_obj.mmcifs[key],
        #         val,
        #         msg="Structure not being stored as Gemmi Block object value in "
        #         "the ClusterConformations.mmcif_paths dict.",
        #     )

        # Check UniProt accession has been saved
        self.assertEqual(
            self.test_obj.unp,
            TEST_UNP,
            msg="UniProt accession has not been saved correctly, or at all. ",
        )

        # Check dictionary of paths has been created correctly
        self.assertDictEqual(self.test_obj.mmcif_paths, EXPECTED_OBJ.mmcif_paths)

        # Check dictionary of chains is created correctly
        self.assertDictEqual(
            self.test_obj.chains,
            EXPECTED_OBJ.chains,
            msg="Chain information is not being stored correctly as values in the "
            "ClusterConformations.chains dict.",
        )

        # Check list of redundant chains
        self.assertTrue(
            np.array_equal(self.test_obj.chains_all, EXPECTED_OBJ.chains_all),
            msg="Redundant list of parsed chains are not being correctly stored in the "
            "ClusterConformations.chains list.",
        )

        # Check list of PDBe IDs are being stored correctly -- v. imp.
        self.assertListEqual(
            self.test_obj.pdbe_ids,
            EXPECTED_OBJ.pdbe_ids,
            msg="Non-redundant list of parsed PDB IDs are not being correctly stored in "
            "the ClusterConformations.pdbe_ids list.",
        )

    ####################################################################################
    # @mock.patch(
    #     "cluster_conformers.cluster_monomers.parsing_utils.parse_mmcif",
    #     # side_effect=[
    #     #     {
    #     #         "cartn_x": [1.0, 2.0, 3.0, 4.0, 5.0],
    #     #         "cartn_y": [1.0, 2.0, 3.0, 4.0, 5.0],
    #     #         "cartn_z": [1.0, 2.0, 3.0, 4.0, 5.0],
    #     #         "unp_res_ids": [1, 2, 3, 4, 5],
    #     #     }
    #     # ]
    #     # * 8,
    # )
    # @mock.patch(
    #     "cluster_conformers.cluster_monomers.linear_algebra_utils.generate_ca_matx",
    #     # side_effect=[
    #     #     np.array(
    #     #         [
    #     #             [0.0, 1.73205081, 3.46410162, 5.19615242, 6.92820323],
    #     #             [1.73205081, 0.0, 1.73205081, 3.46410162, 5.19615242],
    #     #             [3.46410162, 1.73205081, 0.0, 1.73205081, 3.46410162],
    #     #             [5.19615242, 3.46410162, 1.73205081, 0.0, 1.73205081],
    #     #             [6.92820323, 5.19615242, 3.46410162, 1.73205081, 0.0],
    #     #         ]
    #     #     )
    #     # ]
    #     # * 8,
    # )
    @mock.patch("cluster_conformers.cluster_monomers.io_utils.load_mmcif")
    @mock.patch("cluster_conformers.cluster_monomers.parsing_utils.parse_mmcif")
    @mock.patch("cluster_conformers.cluster_monomers.parsing_utils.fill_missing_unps")
    @mock.patch(
        "cluster_conformers.cluster_monomers.linear_algebra_utils.generate_ca_matx"
    )
    ####################################################################################
    def test_ca_distance(
        self,
        mock_ca_matx,
        mock_xyz_unp_dict_filled,
        mock_xyz_unp_dict_unfilled,
        mock_mmcif,
    ):
        """
        Tests the method used to generate CA distances for each parsed chain. This test
        checks if the results are stored as fields to the class instance and also saved
        to the parsed path.

        Running the test will begin by deleting all previously saved results in the save
        path, leaving only the results from the last-submitted test in the folder. To
        keep a record of previous tests, copy the contents of `PATH_SAVE_CA` to a new
        location.
        """

        mock_mmcif.return_value = None

        mock_xyz_unp_dict = {
            "cartn_x": [1.0, 2.0, 3.0, 4.0, 5.0],
            "cartn_y": [1.0, 2.0, 3.0, 4.0, 5.0],
            "cartn_z": [1.0, 2.0, 3.0, 4.0, 5.0],
            "unp_res_ids": [1, 2, 3, 4, 5],
        }
        mock_xyz_unp_dict_unfilled.return_value = mock_xyz_unp_dict
        mock_xyz_unp_dict_filled.return_value = mock_xyz_unp_dict
        mock_ca_matx.return_value = np.array(
            [
                [0.0, 1.73205081, 3.46410162, 5.19615242, 6.92820323],
                [1.73205081, 0.0, 1.73205081, 3.46410162, 5.19615242],
                [3.46410162, 1.73205081, 0.0, 1.73205081, 3.46410162],
                [5.19615242, 3.46410162, 1.73205081, 0.0, 1.73205081],
                [6.92820323, 5.19615242, 3.46410162, 1.73205081, 0.0],
            ]
        )

        # Remove saved files from previous tests
        remove_files(path=PATH_SAVE_CA)

        # Run test
        self.test_obj.ca_distance(PATH_SAVE_CA)

        # Expected outputs
        # expected_ca_matx = np.array(
        #     [
        #         [0.0, 1.73205081, 3.46410162, 5.19615242, 6.92820323],
        #         [1.73205081, 0.0, 1.73205081, 3.46410162, 5.19615242],
        #         [3.46410162, 1.73205081, 0.0, 1.73205081, 3.46410162],
        #         [5.19615242, 3.46410162, 1.73205081, 0.0, 1.73205081],
        #         [6.92820323, 5.19615242, 3.46410162, 1.73205081, 0.0],
        #     ]
        # )
        expected_pdbe_chain_ids = [
            "3nc3_A",
            "3nc3_B",
            "3nc5_A",
            "3nc5_B",
            "3nc6_A",
            "3nc6_B",
            "3nc7_A",
            "3nc7_B",
        ]

        # Iteration needed here as values stored as arrays
        for pdbe_chain_id in expected_pdbe_chain_ids:

            # Check all expected keys have been stored to dictionary
            self.assertTrue(pdbe_chain_id in self.test_obj.ca_matxs.keys())

            # Check matrix was saved correctly
            self.assertIsFile(
                PATH_SAVE_CA.joinpath(str(pdbe_chain_id) + "_ca_distance_matrix.npz")
            )

        # Test clustering now

        # Remove saved files from previous tests
        # remove_files(PATH_SAVE_CLUSTER)
        # self.test_obj.cluster(path_save_cluster_results=PATH_SAVE_CLUSTER)

    ####################################################################################
    @mock.patch(
        "cluster_conformers.cluster_monomers.cluster_chains.build_inputs",
    )
    @mock.patch(
        "cluster_conformers.cluster_monomers.cluster_chains.cluster_agglomerative"
    )
    @mock.patch("cluster_conformers.cluster_monomers.cluster_chains.make_linkage_matx")
    ####################################################################################
    def test_cluster(self, mock_linkage_matx, mock_model, mock_build_inputs):
        """
        Testing the ClusterConformations.cluster() method, using arbitrary CA distance,
        score and linkage matrices. The functions used to generate these are tested
        elsewhere. This tests to check if the method can construct the DataFrame field
        needed for saving to CSV and other downstream methods.

        Test will begin by deleting all files saved from previous tests, which store
        results to the `PATH_SAVE_CLUSTER` directory. To keep a record of earlier
        tests, copy the contents of `PATH_SAVE_CLUSTER` to a new location.

        # TODO -- add check for case where score matrix is complete zero.
        """

        # Remove saved files from previous tests
        remove_files(PATH_SAVE_CLUSTER)

        # Add missing fields to class instance -- would have been generated if
        # ca_distances() was run first
        self.test_obj.pdbe_chain_ids = np.array(
            [
                "3nc3_A",
                "3nc3_B",
                "3nc5_A",
                "3nc5_B",
                "3nc6_A",
                "3nc6_B",
                "3nc7_A",
                "3nc7_B",
            ]
        )

        self.test_obj.unp_res_ids = None
        self.test_obj.ca_matxs = None

        # Define patch objects
        # Returned from:  cluster_chains.build_inputs
        mock_build_inputs.return_value = (
            np.array(  # Score matrix: self.score_matx
                [  # Random array of integers in symmetric matrix
                    [24663, 27079, 14435, 15810, 18835, 22947, 12822, 24241],
                    [27079, 44155, 26517, 24048, 30788, 28742, 22596, 28651],
                    [14435, 26517, 19650, 14055, 17937, 16478, 11982, 14145],
                    [15810, 24048, 14055, 18928, 21388, 14642, 14179, 18416],
                    [18835, 30788, 17937, 21388, 26642, 17607, 16876, 21264],
                    [22947, 28742, 16478, 14642, 17607, 26829, 13487, 21505],
                    [12822, 22596, 11982, 14179, 16876, 13487, 13953, 15661],
                    [24241, 28651, 14145, 18416, 21264, 21505, 15661, 28686],
                ]
            ),
            np.array(  # Label matrix: self.label_matx
                [
                    [
                        "3nc3_A_to_3nc3_A",
                        "3nc3_A_to_3nc3_B",
                        "3nc3_A_to_3nc5_A",
                        "3nc3_A_to_3nc5_B",
                        "3nc3_A_to_3nc6_A",
                        "3nc3_A_to_3nc6_B",
                        "3nc3_A_to_3nc7_A",
                        "3nc3_A_to_3nc7_B",
                    ],
                    [
                        "3nc3_B_to_3nc3_A",
                        "3nc3_B_to_3nc3_B",
                        "3nc3_B_to_3nc5_A",
                        "3nc3_B_to_3nc5_B",
                        "3nc3_B_to_3nc6_A",
                        "3nc3_B_to_3nc6_B",
                        "3nc3_B_to_3nc7_A",
                        "3nc3_B_to_3nc7_B",
                    ],
                    [
                        "3nc5_A_to_3nc3_A",
                        "3nc5_A_to_3nc3_B",
                        "3nc5_A_to_3nc5_A",
                        "3nc5_A_to_3nc5_B",
                        "3nc5_A_to_3nc6_A",
                        "3nc5_A_to_3nc6_B",
                        "3nc5_A_to_3nc7_A",
                        "3nc5_A_to_3nc7_B",
                    ],
                    [
                        "3nc5_B_to_3nc3_A",
                        "3nc5_B_to_3nc3_B",
                        "3nc5_B_to_3nc5_A",
                        "3nc5_B_to_3nc5_B",
                        "3nc5_B_to_3nc6_A",
                        "3nc5_B_to_3nc6_B",
                        "3nc5_B_to_3nc7_A",
                        "3nc5_B_to_3nc7_B",
                    ],
                    [
                        "3nc6_A_to_3nc3_A",
                        "3nc6_A_to_3nc3_B",
                        "3nc6_A_to_3nc5_A",
                        "3nc6_A_to_3nc5_B",
                        "3nc6_A_to_3nc6_A",
                        "3nc6_A_to_3nc6_B",
                        "3nc6_A_to_3nc7_A",
                        "3nc6_A_to_3nc7_B",
                    ],
                    [
                        "3nc6_B_to_3nc3_A",
                        "3nc6_B_to_3nc3_B",
                        "3nc6_B_to_3nc5_A",
                        "3nc6_B_to_3nc5_B",
                        "3nc6_B_to_3nc6_A",
                        "3nc6_B_to_3nc6_B",
                        "3nc6_B_to_3nc7_A",
                        "3nc6_B_to_3nc7_B",
                    ],
                    [
                        "3nc7_A_to_3nc3_A",
                        "3nc7_A_to_3nc3_B",
                        "3nc7_A_to_3nc5_A",
                        "3nc7_A_to_3nc5_B",
                        "3nc7_A_to_3nc6_A",
                        "3nc7_A_to_3nc6_B",
                        "3nc7_A_to_3nc7_A",
                        "3nc7_A_to_3nc7_B",
                    ],
                    [
                        "3nc7_B_to_3nc3_A",
                        "3nc7_B_to_3nc3_B",
                        "3nc7_B_to_3nc5_A",
                        "3nc7_B_to_3nc5_B",
                        "3nc7_B_to_3nc6_A",
                        "3nc7_B_to_3nc6_B",
                        "3nc7_B_to_3nc7_A",
                        "3nc7_B_to_3nc7_B",
                    ],
                ]
            ),
        )

        # This is the model which should be returned give the above score matrix
        mock_model.return_value = mock.Mock(
            name="Mock AgglomerativeClustering model",
            **{
                "distances_": np.array(
                    [
                        11982.0,
                        13628.5,
                        14642.0,
                        16159.33333333,
                        18528.6,
                        19205.33333333,
                        26917.28571429,
                    ]
                ),
                "labels_": np.array([0, 1, 0, 0, 0, 0, 0, 2]),
                "children_": np.array(
                    [[2, 6], [0, 8], [3, 5], [9, 10], [4, 11], [7, 12], [1, 13]]
                ),
            },
        )

        # This is the linkage matrix which should be returned given the above mock model
        mock_linkage_matx.return_value = np.array(
            [
                [2.00000000e00, 6.00000000e00, 1.19820000e04, 2.00000000e00],
                [0.00000000e00, 8.00000000e00, 1.36285000e04, 3.00000000e00],
                [3.00000000e00, 5.00000000e00, 1.46420000e04, 2.00000000e00],
                [9.00000000e00, 1.00000000e01, 1.61593333e04, 5.00000000e00],
                [4.00000000e00, 1.10000000e01, 1.85286000e04, 6.00000000e00],
                [7.00000000e00, 1.20000000e01, 1.92053333e04, 7.00000000e00],
                [1.00000000e00, 1.30000000e01, 2.69172857e04, 8.00000000e00],
            ]
        )

        # Run the method
        self.test_obj.cluster(
            path_save_dd_matx=PATH_DD_MATXS, path_save_cluster_results=PATH_SAVE_CLUSTER
        )

        # Check a Pandas DataFrame is returned
        self.assertTrue(
            type(self.test_obj.cluster_df) == pd.DataFrame,
            msg="Clustering results are not being saved as a Pandas DataFrame",
        )

        # Check minimum number of columns have been created
        for col_name in ["UNP_ACC", "PDBe_ID", "CHAIN_ID", "CONFORMER_ID"]:
            self.assertTrue(
                col_name in self.test_obj.cluster_df.columns,
                msg=f"Clustering DataFrame is missing column {col_name}",
            )

        # Check at least one row is correctly formatted
        row_index = 0
        self.assertEqual(  # UniProt accession
            self.test_obj.cluster_df.iloc[row_index]["UNP_ACC"],
            TEST_UNP,
            msg="UniProt accession has not been stored correctly",
        )

        self.assertTrue(  # Check PDB ID is stored as a string
            type(self.test_obj.cluster_df.iloc[row_index]["PDBe_ID"]) == str,
            msg="PDB ID has not been stored as a string",
        )

        self.assertTrue(  # Check chain ID stored as a single string
            type(self.test_obj.cluster_df.iloc[row_index]["CHAIN_ID"]) == str
            and len(self.test_obj.cluster_df.iloc[row_index]["CHAIN_ID"]) == 1,
            msg="Chain ID has not been stored as a single character",
        )

        self.assertTrue(  # Check chain ID stored as a single string
            self.test_obj.cluster_df.iloc[row_index]["CHAIN_ID"]
            != self.test_obj.cluster_df.iloc[row_index]["CHAIN_ID"].lower(),
            msg="Chain ID has not been stored in upper case. Important to keep this "
            "consistent for downstream analyses",
        )

        # Expected files saved from clustering process
        expected_output_files = [
            f"{TEST_UNP}_sum_based_clustering_results.csv",
            f"{TEST_UNP}_score_matrix.npz",
            f"{TEST_UNP}_label_matrix.npz",
            f"{TEST_UNP}_linkage_matrix.npz",
        ]

        # Check expected files have been saved
        for file in expected_output_files:
            path_to_expected_file = PATH_SAVE_CLUSTER.joinpath(file)
            self.assertIsFile(path_to_expected_file)

        """
        The make_dendrogram() method is now run as this current instance of the
        ClusterConformations() object now contains the prerequisite attributes after
        executing ClusterConformations().cluster()
        """
        # Run method
        self.test_obj.make_dendrogram(path_save=PATH_SAVE_CLUSTER, png=True, svg=True)

        # Check PNG file has been created
        self.assertIsFile(
            PATH_SAVE_CLUSTER.joinpath(f"{TEST_UNP}_agglomerative_dendrogram.png")
        )

        # Check SVG file has been created
        self.assertIsFile(
            PATH_SAVE_CLUSTER.joinpath(f"{TEST_UNP}_agglomerative_dendrogram.svg")
        )

        """
        The make_swarmplot() method is now run as this current instance of the
        ClusterConformations() object now contains the prerequisite attributes after
        executing ClusterConformations().cluster()
        """
        # Run method
        self.test_obj.make_swarmplot(path_save=PATH_SAVE_CLUSTER, png=True, svg=True)

        # Check PNG file has been created
        self.assertIsFile(PATH_SAVE_CLUSTER.joinpath(f"{TEST_UNP}_swarm_plot.png"))

        # Check SVG file has been created
        self.assertIsFile(PATH_SAVE_CLUSTER.joinpath(f"{TEST_UNP}_swarm_plot.svg"))

        # # Test select_representatives() method -- remove in favour of test method below
        # # later
        # self.test_obj.select_representatives()

    # ####################################################################################

    # ####################################################################################
    # def test_select_representatives(self):
    #     """
    #     The select_representatives() method is now run as this current instance of the
    #     ClusterConformations() object now contains the prerequisite attributes after
    #     executing ClusterConformations().cluster()
    #     """
    #     # # Run method
    #     # self.test_obj.select_representatives()

    #     # # Find number of selected representatives from the clustering results
    #     # num_representatives = 0
    #     # for _, value in self.test_obj.representatives.items():
    #     #     if value:
    #     #         num_representatives += 1
    #     # print(num_representatives)

    #     # # Check the method is selecting the same number of representatives as
    #     # # predicted clusters
    #     # self.assertEqual(
    #     #     num_representatives,
    #     #     len(self.test_obj.cluster_df["CONFORMER_ID"].unique()), # Num pred. clusters.
    #     #     msg=f"The number of representatives selected {num_representatives} does "
    #     #         "not match the number of clusters"
    #     # )
    #     pass

    # ####################################################################################
    # @mock.patch(
    #     "cluster_conformers.cluster_monomers.linear_algebra_utils.upper_triangle",
    #     side_effect=[
    #         # Distance difference matrices
    #         np.array(
    #             [
    #                 PATH_SERIALISED_MATXS.joinpath("matx_1.pickle"),
    #                 PATH_SERIALISED_MATXS.joinpath("matx_2.pickle"),
    #                 PATH_SERIALISED_MATXS.joinpath("matx_3.pickle"),
    #             ]
    #         ),
    #         # Label array
    #         np.array(["1pdb_A_to_2pdb_A", "1pdb_A_to_2pdb_B", "2pdb_A_to_2pdb_B"]),
    #         # Score array
    #         np.array([200, 300, 400]),
    #     ],
    # )
    # @mock.patch(
    #     "cluster_conformers.cluster_monomers.plot_distance_differences.make_heatmap_kwargs"
    # )
    # @mock.patch(
    #     "cluster_conformers.cluster_monomers.linear_algebra_utils.find_max",
    #     side_effect=[16] * 3,
    # )
    # ####################################################################################
    # def test_make_dd_maps(
    #     self, mock_max_distance, mock_heatmap_kwargs, mock_uppertriangles
    # ):

    #     # Mocking missing attributes
    #     self.test_obj.dd_matxs = np.full((3, 3), np.full((3, 3), 1))
    #     self.test_obj.label_matx = np.full((3, 3), "title")
    #     self.test_obj.score_matx = np.full((3, 3), 100)
    #     self.test_obj.cluster_df = pd.DataFrame(
    #         {
    #             "UNP_ACC": [TEST_UNP] * 3,
    #             "PDBe_ID": ["1pdb", "2pdb", "2pdb"],
    #             "CHAIN_ID": ["A", "A", "B"],
    #             "CONFORMER_ID": [0, 1, 1],
    #         }
    #     )

    #     self.test_obj.make_dd_maps(path_save=PATH_SAVE_DD_MAPS)

    #     # Check files have been rendered and saved
    #     expected_files = [
    #         f"{TEST_UNP}_1pdb_A_to_2pdb_A.png",
    #         f"{TEST_UNP}_1pdb_A_to_2pdb_B.png",
    #         f"{TEST_UNP}_2pdb_A_to_2pdb_B.png",
    #     ]

    #     # Check files have been saved
    #     for file in expected_files:
    #         self.assertIsFile(PATH_SAVE_DD_MAPS.joinpath(file))


# Run unit tests on call of script
if __name__ == "__main__":

    unittest.main()
